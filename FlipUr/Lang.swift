//
//  Lang.swift
//  MyLang
//
//  Created by 竹野学 on 2017/05/12.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation

class Lang {
    
    
//    static let TITLE_TRANSACTION : String = ""
    static let COMPLETED_REGISTRATION_TRANSACTION : String = "登録が完了しました。"
    static let COMPLETED_REMOVE_FROM_MYLIST_TRANSACTION : String = "Listから削除しました。"
    static let COMPLETED_DELETE_FROM_MYLIST_TRANSACTION : String = "Listから削除しました。"
    static let ERROR_REQUIRED : String = "文字を入力してください。"
    static let COMPLETED_BOOKMARK_TRANSACTION : String = "Book Markしました。"
    
    static let COMPLETED_REMEMBER_TRANSACTION : String = "Rememberに登録しました。"
    static let ALREADY_REMEMBERED_TRANSACTION : String = "既にRememberListに登録されています。"
    static let ALREADY_BOOKMARKED_TRANSACTION : String = "既にBookMarkListに登録されています。"
    
    
    static let COMPLETED_REPORT_TRANSACTION : String = "このアイテムを通報しました。"
    static let COMPLETED_BLOCK_TRANSACTION : String = "このアイテムをブロックしました。"
    
    static let ERROR_PLEASE_LOGIN : String = "ログインしてください。"
    
    static let ERROR_OFFLINE : String = "ネットに繋がれておりません。"
    
    static let ERROR_NO_DATA : String = "データがありません。"
    static let ERROR_UNEXPECTED_ERROR_OCCURED : String = "予期せぬエラーが発生しました。"
    
    static let OK : String = "OK"
    static let CANCEL : String = "Cancel"
    static let CONFIRM_TITLE : String = "確認"
    static let CONFIRM_BLOCK : String = "この記事をブロックしますか？"
    static let CONFIRM_REPORT : String = "この記事を通報しますか？"
    
}
