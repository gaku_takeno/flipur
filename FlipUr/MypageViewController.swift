//
//  MypageViewController.swift
//  MyLang
//
//  Created by 竹野学 on 2017/05/16.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKCoreKit

import FacebookLogin
import RealmSwift
import Alamofire
import SwiftyJSON
import RealmSwift
import SDWebImage

class MypageViewController: UIViewController {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var faceBookButton: UIButton!
    @IBOutlet weak var myListButton: UIButton!
    @IBOutlet weak var userIcon: UIImageView!
    
    var status = 0 //0 ログアウト 1 ログイン
    var user_id = 0
    var name = ""
    var snsid = ""
    
    @IBAction func onFacebookLoginLogout(_ sender: Any) {
        if ( self.status == 0) {
            facebookLogin()
        } else {
            fbLogout()
        }
        
        Common.setIconStatusChange(val:1)
        Common.setMainStatusChange(val:1)
        Common.setBookmarkStatusChange(val:1)
        Common.setRememberStatusChange(val:1)
        
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Mypage"
        
        
        //fbログイン中
        if Common.isLoggedInWithFacebook() == true {
            if (!isRegisterUserRealm()) {
                getUserInfoFromApi()
            }
            
            self.inLogin()
            
        } else {
            
            self.inLogout()
            
        }
        
    }
    
    
    /**
     * Mywordテーブルのデータを全て消す
    */
    private func allMywordsDelete () {
        
        // Realmのインスタンスを取得
        let realm = try! Realm()
        
        // Realmに保存されてるDog型のオブジェクトを全て取得
        let words = realm.objects(Myword.self)
        
        // ためしに名前を表示
        for word in words {
            // さようなら・・・
            try! realm.write() {
                realm.delete(word)
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /**
     * FBログイン
     */
    private func facebookLogin() {
        
        LoginManager().logIn([.email], viewController: self, completion: {
            result in
            switch result {
            case let .success( permission, declinePemisson, token):
                
                self.getUserInfoFromApi()
                
                
                
            case let .failed(error):
                print("error:::\(error)")
            case .cancelled:
                print("cancelled")
            }
            
        })
    }
    
    /**
    * FBログアウト
    */
    private func fbLogout() {
        LoginManager().logOut()
        
        self.inLogout()
        
        self.allMywordsDelete()
        
//        Common.setLoginStatusChange(val:1)

        
    }
    
    
    /**
    * ログイン中のviewを設定する
    */
    private func inLogin () {
      
        
        self.statusLabel.text = "ログイン中"
        self.faceBookButton.setTitle("ログアウトする", for: .normal) // ボタンのタイトル
//        self.faceBookButton.setTitleColor(MyColors.SKY_BLUE, for: .normal) // タイトルの色
        self.status = 1
        self.myListButton.isHidden = false
        
        
        //ユーザーアイコンを表示する
        let urlpath = "https://graph.facebook.com/"+self.snsid+"/picture?type=large"
        let imageURL = URL(string: urlpath)
        userIcon.sd_setImage(with: imageURL)
        
//        userIcon.clipsToBounds = true;
        userIcon.layer.cornerRadius = 20.0;

        
    }
    
    
    /**
     * ログアウト中のviewを設定する
     */
    private func inLogout () {
        
        
        self.statusLabel.text = "ログアウト中"
        self.faceBookButton.setTitle("ログインする", for: .normal) // ボタンのタイトル
//        self.faceBookButton.setTitleColor(MyColors.SKY_BLUE, for: .normal) // タイトルの色
        self.status = 0
        
        self.myListButton.isHidden = true
        
        //ユーザーアイコンを表示する
        let cellImage = UIImage(named: "background")
        userIcon.image = cellImage
        
//        Common.setLoginStatusChange(val:self.status)
        
    }
    
    
    
    
    
    /**
     * ローカルユーザー情報が登録されているかどうかをチェック
    */
    private func isRegisterUserRealm () -> Bool{
        
        let realm = try! Realm()
        let obj = realm.objects(User.self)
        if obj.count > 0 {
            
            let inf = obj.first
            self.name = (inf?.name)!
            self.user_id = (inf?.id)!
            self.snsid = (inf?.snsid)!
            
            self.inLogin()
            

            return false
        } else {
            
            self.inLogout()
            return true
        }
        
    }
    
    
    
    func registerUserInfo (userInfo: (snsid:String, name:String, email:String)) {
        
        snsid = userInfo.snsid
        name = userInfo.name
      
        let params: [String: Any] = ["snsid":snsid,"name":name]
        
        Alamofire.request(Constants.URL_REGIST_USERS, method: .post, parameters: params, encoding: URLEncoding.httpBody).responseJSON
            {
                response in
                
                if let data = response.data {
                    
                    let json = JSON(data)
                    
                    let resultCode = json["ResultCode"].int
                    if (resultCode == 0) {
                        
                        var results = json["Results"]
                        
  
                        let realm = try! Realm()
                        
                        let obj = realm.objects(User.self).filter("snsid = '"+self.snsid+"'")
                        if obj.count == 0 {

                            let userid:Int = results["userid"].int!
                            
                            /* 書き込み */
                            let user = User()
                            user.id  = userid
                            user.snsid  = self.snsid
                            user.name = self.name
                            
                            try! realm.write() {
                                realm.add(user)
                            }
                            

                        } else {
                            //更新
                            
                            let userObj = obj.first
                            try! realm.write {
                                userObj!.name = self.name
                                userObj!.snsid = self.snsid
                                
                            }
                            
                        }
                        
                        self.inLogin()
                        
                        
                    }
                    
                }

            }
        
        
    }
    
    
    
    /**
     * ローカル(realm)からユーザー情報を取得する
     */
    private func getUserInfo(){
        
        
    }
    
    /**
     * クライドAPIからユーザー情報を取得する
     */
    private func getUserInfoFromApi (){
        
        //Facebookのユーザー情報を取得する処理
        GraphRequest(graphPath: "me", parameters: ["fields": "name, email"], accessToken: AccessToken.current, httpMethod: .GET, apiVersion: GraphAPIVersion.defaultVersion).start({
            response, result in
            switch result {
            case .success(let response) :
                
                self.snsid = (response.dictionaryValue?["id"] as? String)!
                self.name = (response.dictionaryValue?["name"] as? String)!
                let email = response.dictionaryValue?["email"] as? String
//                print(self.snsid)
                //オプショナルバインディング
                //if文の条件文がnilかどうかを判別する
                //nilじゃなければ、true
                //右辺の変数は必ずオプショナルである必要あり。
                //「,」は 論理演算子 の論理積（AND）の働きをする
                if self.snsid != nil , self.name != nil , email != nil {
                    let userInfo = (snsid:self.snsid,name:self.name,email:email)
                    self.registerUserInfo(userInfo: userInfo as! (snsid:String, name:String, email:String))
                    
                }
                
                break
                
            case .failed(let error):
                print("error:｜｜｜｜｜\(error.localizedDescription)")
                
            }
            
        })
    }
    
    
    
}
