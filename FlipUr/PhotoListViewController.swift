//
//  PhotoListViewController.swift
//  FlipUr
//
//  Created by 竹野学 on 2017/06/15.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import UIKit
import AVFoundation

final class PhotoListViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout,UIScrollViewDelegate {
    
//    @IBOutlet weak var photoListCollectionView: UICollectionView!
    @IBOutlet weak var photoListCollectionView: UICollectionView!
    
    
//    var presenter: PhotoListPresenter?
//    var photoListDataSource = PhotoListCollectionView()
    var photos: [Photo] = []
    var presenter: WordPresenter?
    var common:Common!
    var words = [Word]()
    var count = 0
    var is_last:Int = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "My List"
        presenter = WordPresenter(view: self)
        presenter?.loadMyList(type:Constants.MYLIST_TYPE_NORMAL)
        
        if let layout = photoListCollectionView.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        // Cell はストーリーボードで設定したセルのID
        let collectionCell:PhotoListCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoListCell", for: indexPath) as! PhotoListCell
        
//        collectionCell.word = words[indexPath.row]
        
//        let word = words[(indexPath as NSIndexPath).row]
//        word.indexPath = indexPath
        collectionCell.setCell(word:words[indexPath.row])
//
//        
//        
        return collectionCell
        
    }
    
    
    // Screenサイズに応じたセルサイズを返す
    // UICollectionViewDelegateFlowLayoutの設定が必要
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellSizeW:CGFloat = common.getWidth()
        let h = common.getHeight() - CGFloat(Constants.NAVIGATION_HEIGHT) - CGFloat(Constants.MENU_BAR_HEIGHT)
        let cellSizeH:CGFloat = h
        
        // 正方形で返すためにwidth,heightを同じにする
        return CGSize(width: cellSizeW, height: cellSizeH)
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        //最後の要素で且つ、続きのデータがある場合
        if indexPath.row == words.count - 1, self.is_last == 0 {
            presenter?.loadMyList(type:Constants.MYLIST_TYPE_NORMAL)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // 要素数を入れる、要素以上の数字を入れると表示でエラーとなる
        return words.count;
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

}

//MARK:- PinterestLayoutDelegate
extension PhotoListViewController: PinterestLayoutDelegate {
    
    func collectionView(_ collectionView:UICollectionView,
                        heightForPhotoAtIndexPath indexPath:IndexPath ,
                        withWidth width:CGFloat) -> CGFloat {
        
        let collectionCell:PhotoListCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoListCell", for: indexPath) as! PhotoListCell
        
        let photo = words[indexPath.row]
        let boundingRect = CGRect(x: 0, y: 0, width: width, height: CGFloat(MAXFLOAT))
        let rect = AVMakeRect(aspectRatio: collectionCell.thumnailImageView.frame.size, insideRect: boundingRect)
        return rect.size.height
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        heightForCaptionAndCommentAtIndexPath indexPath: IndexPath,
                        withWidth width: CGFloat) -> CGFloat {
        
        let photo = words[(indexPath as NSIndexPath).item]
        
        let padding = CGFloat(16)
        let captionrHeight = photo.heightForCaption(UIFont.systemFont(ofSize: 13), width: width)
        let commentHeight = photo.heightForComment(UIFont.systemFont(ofSize: 11), width: width)
        
        let height = padding + captionrHeight + commentHeight + padding
        return height
    }
}




/**
 * ワードリストを取得した後に実行されるコールバックメソッドを実装する。
 */
extension PhotoListViewController: WordViewDelegate {
    
    func onLoaded(words:[Word],is_last: Int) {
        
        print(words.count.description+":::::")
        self.words += words
        setScrollView()
    }
    func onReloaded(words:[Word]) {}
    func onRegistered(word:Word) {}
    func onEdited(word:Word) {}
    
    func onDeleted(pageIndex:Int,word:Word) {}
    func onBookMarked(word: Word,message:String) {}
    func onRemembered(word:Word,message:String) {}
    func onRemovedFromMyList(word: Word, message: String) {}
}

/**
 * scrollviewの描画を行う。
 */
extension PhotoListViewController {
    
    func setScrollView() {
        photoListCollectionView.reloadData()

    }
}

