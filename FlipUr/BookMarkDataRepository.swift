//
//  DataStore.swift
//  MyUiScrollView
//
//  Created by 竹野学 on 2017/04/18.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift

class BookMarkDataRepository {
    
    var word = Word()
    var usecase: WordUsecaseDelegate
    var common:Common
    let myword:Myword
    
    init(usecase:WordUsecaseDelegate) {
        self.usecase = usecase
        self.common = Common()
        self.common.setUserInfo()
        self.myword = Myword()
        
    }
    
    /**
     * bookmarkを開始
     */
    func bookMarkWord(word:Word){
        
        self.word = word
        
        //オンラインの場合
        if (Common.isInternetAvailable()) {
            registerDataFromOnLine()
        }
        
    }
    
    
    private func registerDataFromOnLine () {

        let params: [String: Any] = ["word_id":word.word_id,"comment1":word.comment1,"comment2":word.comment2,"auther_user_id":common.getUserId()]
        
        Alamofire.request(Constants.URL_BOOKMARK_WORDS_USERS, method: .post, parameters: params, encoding: URLEncoding.httpBody).responseJSON
            {
                response in
                
                self.loadedData(response: response)
                
        }
        
        
    }
//    
//    private func getDataFromOffLine () {
//        
//    }
    
    private func loadedData(response: DataResponse<Any>?){
        
        if let data = response?.data {
            
            let json = JSON(data)
            
            let resultCode = json["ResultCode"].int
            
            let message = json["Message"]
            print(message.description+"::")
            if (resultCode == 0) {
                
                var results = json["Results"]
                let dataArg = results["Data"]
                let user_id:Int = dataArg["user_id"].int!
                self.myword.user_id = user_id
                self.bookMarkInRealm()
                
                self.usecase.bookMarkDone(message:Lang.COMPLETED_BOOKMARK_TRANSACTION)
            
            } else if (resultCode == 2) {
                self.usecase.bookMarkDone(message:Lang.ALREADY_BOOKMARKED_TRANSACTION)
                
            } else {
                self.usecase.bookMarkDone(message:Lang.ERROR_UNEXPECTED_ERROR_OCCURED)
                
            }
            
        }
        
    }
    
    
}


/**
 * ローカル側にもデータを登録する
 */
extension BookMarkDataRepository {
    
    func bookMarkInRealm() {
        
        let realm = try! Realm()
        
        
        let filterCon = "id = " + word.word_id.description
        let obj = realm.objects(Myword.self).filter(filterCon)
        let userObj = obj.first
        if obj.count == 0 {
            
            print("bookmarked!")
        
            let mywordObj = Myword()
            /* 書き込み */
            mywordObj.id  = word.word_id
            mywordObj.comment1  = word.comment1
            mywordObj.comment2  = word.comment2
            mywordObj.auther_user_id = common.getUserId()
            mywordObj.is_bookmarked = 1
            mywordObj.is_remembered = 0
            try! realm.write() {
                realm.add(mywordObj)
            }
       
        } else {
            
            
            try! realm.write {
                userObj?.is_bookmarked = 1
            }
            print("bookmarked!+update")
            
        }
        
        
    }
}

//
//
///**
// * ワードリストを取得した後に実行されるコールバックメソッドを実装する。
// */
//extension RegisterViewController: WordViewDelegate {
//
//    func onLoaded(words:[Word]) {
//
////        self.words += words
////        count = self.words.count
////
////        //scrollviewの描画を行う
////        setScrollView()
//
//    }
//
//}

