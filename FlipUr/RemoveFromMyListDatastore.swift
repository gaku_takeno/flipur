//
//  DataStore.swift
//  MyUiScrollView
//
//  Created by 竹野学 on 2017/04/18.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift

class RemoveFromMyListDataRepository {
    
    var word = Word()
    var usecase: WordUsecaseDelegate
    var common:Common
    let myword:Myword
    var list_type:Int = 0
    
    init(usecase:WordUsecaseDelegate) {
        self.usecase = usecase
        self.common = Common()
        self.common.setUserInfo()
        self.myword = Myword()
        
        
    }
    
    /**
     * remove From MyListを開始
     */
    func removeFromMyListWord(word:Word,list_type:Int){
        
        self.word = word
        self.list_type = list_type
        
        //オンラインの場合
        if (Common.isInternetAvailable()) {
            removeDataFromOnLine()
        } else {
            print("offline")
        }
        
    }
    
    
    private func removeDataFromOnLine () {
        
        let params: [String: Any] = ["word_id":word.word_id,"list_type":self.list_type]
        
        Alamofire.request(Constants.URL_REMOVE_FROM_MYLIST, method: .post, parameters: params, encoding: URLEncoding.httpBody).responseJSON
            {
                response in
                
                self.loadedData(response: response)
                
        }
        
        
    }
    
    private func getDataFromOffLine () {
        
    }
    
    private func loadedData(response: DataResponse<Any>?){
        
        if let data = response?.data {
            
            let json = JSON(data)
            
            let resultCode = json["ResultCode"].int
            
            var message = json["Message"]
            if (resultCode == 0) {
                
                var results = json["Results"]
                let dataArg = results["Data"]
//                let user_id:Int = dataArg["user_id"].int!
//                self.myword.user_id = user_id
                self.removeInRealm()
                
                self.usecase.removeFromMyListDone(message:Lang.COMPLETED_REMOVE_FROM_MYLIST_TRANSACTION)
                
                
            } else {
                self.usecase.removeFromMyListDone(message:Lang.ERROR_UNEXPECTED_ERROR_OCCURED)
            }
            
        }
        
    }
    
    
}


/**
 * ローカル側のデータも削除する
 */
extension RemoveFromMyListDataRepository {
    
    func removeInRealm() {
        
        let realm = try! Realm()
        
        
        let filterCon = "id = " + word.word_id.description
        let obj = realm.objects(Myword.self).filter(filterCon)
        
        
        if obj.count > 0 {
        
            let userObj = obj.first
            
            //bookmark
            if (self.list_type == 1 && userObj!.is_remembered == 1) {
            
                try! realm.write {
                    userObj!.is_bookmarked = 0
                    
                    print("bookmarkを削除:update")
                    
                }
                
            } else if (self.list_type == 2 && userObj!.is_bookmarked == 1) {
            //remember
                
                try! realm.write {
                    userObj!.is_remembered = 0
                    print("rememberを削除:update")
                }
                
            } else {
            
                try! realm.write() {
                    realm.delete(obj)
                    print("削除:delete")
                }
            }
            
            let objects = realm.objects(Myword.self)
            print(objects.count.description+"個になりました。")
            
        }
        
    }
}

