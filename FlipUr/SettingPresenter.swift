//
//  PhotoListPresenter.swift
//  MyUiScrollView
//
//  Created by 竹野学 on 2017/04/18.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation


final class SettingPresenter {
    
    var usecase: SettingUsecase?
    var view:SettingViewDelegate
    
    
    init(view: SettingViewDelegate) {
        self.view = view
        usecase = SettingUsecase(presenter: self)
    }
    
    
    /**
     * 通報開始
     */
    func reportWords(v:Word) {
        
        usecase?.reportWords(word:v)
    }
    
    /**
     * 通報完了
     */
    func onReported(word:Word,message:String) {
        self.view.onReported(word: word,message:message)
    }
    
    /**
     * ブロック開始
     */
    func blockWords(v:Word) {
        
        usecase?.blockWords(word:v)
    }
    
    /**
     * ブロック完了
     */
    func onBlocked(word:Word,message:String) {
        self.view.onBlocked(word: word, message: message)
    }
    

    
}
