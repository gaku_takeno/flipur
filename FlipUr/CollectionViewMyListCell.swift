//
//  CollectionViewCell.swift
//  MyLang
//
//  Created by 竹野学 on 2017/05/09.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation

class CollectionViewMyListCell: UICollectionViewCell {
    
    @IBOutlet weak var comment1: UILabel!
    @IBOutlet weak var comment2: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
//    @IBOutlet weak var isRemembered: UIButton!
//    @IBOutlet weak var isBookmarked: UIButton!
    
    @IBOutlet weak var isYours: UIImageView!
    @IBOutlet weak var removeButton: UIButton!
    var nodataButton:UIButton!
    
    var word = Word()
    var presenter:WordPresenter?
    
    //0:bookmark 1:remember
    var mode:Int = 0
    
    /**
     * 0:bookmark 1:remember
    */
    func setMode(mode:Int) {
        self.mode = mode
    }
    
    func setCell(word: Word,presenter:WordPresenter) {
        
        let common = Common()
        common.setUserInfo()
        
        self.word = word
        self.presenter = presenter
        
        if (Common.isLoggedInWithFacebook() == false) {
            
            self.displayOffLineView(message:Lang.ERROR_PLEASE_LOGIN)
            comment1.isHidden = true
            comment2.isHidden = true
            backgroundImage.isHidden = true
            removeButton.isHidden = true
//            isBookmarked.isHidden = true
//            isRemembered.isHidden = true
            isYours.isHidden = true
            
        } else if (self.word.is_no_data == 1) {
            self.displayOffLineView(message:Lang.ERROR_OFFLINE)
            comment1.isHidden = true
            comment2.isHidden = true
            backgroundImage.isHidden = true
            removeButton.isHidden = true
//            isBookmarked.isHidden = true
//            isRemembered.isHidden = true
            isYours.isHidden = true
            
        } else {
            
            if let b = nodataButton {
                b.removeFromSuperview()
                
            }

            comment1.isHidden = false
            comment2.isHidden = false
            backgroundImage.isHidden = false
            removeButton.isHidden = false
//            isBookmarked.isHidden = false
//            isRemembered.isHidden = false
            isYours.isHidden = false
        
            if (word.image_path != "") {
            
                let imageURL = URL(string: word.image_path)
                backgroundImage.sd_setImage(with: imageURL)

            } else {
        
                //bookmarkの場合
                if (self.mode == 1) {
                    
                    if (Common.isInternetAvailable()) {
                        
                        let imageURL = URL(string: Constants.URL_BOOKMARK_DEFAULT_IMAGE_PATH)
                        backgroundImage.sd_setImage(with: imageURL)
                        
                    } else {
                        
                        let cellImage = UIImage(named: "background2")
                        backgroundImage.image = cellImage
                        
                    }
                    
                } else {
                    
                    if (Common.isInternetAvailable()) {
                        
                        let imageURL = URL(string: Constants.URL_REMEMBER_DEFAULT_IMAGE_PATH)
                        backgroundImage.sd_setImage(with: imageURL)
                        
                    } else {
                        
                        let cellImage = UIImage(named: "background")
                        backgroundImage.image = cellImage
                        
                    }
                    
                }
                
            
            }
        
            comment1.text = word.comment1
            comment2.text = word.comment2
        
//            var image:UIImage!
//            if (self.word.is_bookmarked == 1) {
//                image = UIImage(named: "bookmarkActive")
//                isBookmarked.setImage(image, for: .normal)
//            
//            } else {
//                image = UIImage(named: "bookmark")
//                isBookmarked.setImage(image, for: .normal)
//            
//            }
//        
//            if (self.word.is_remembered == 1) {
//                image = UIImage(named: "remembered")
//                isRemembered.setImage(image, for: .normal)
//            } else {
//                image = UIImage(named: "remember")
//                isRemembered.setImage(image, for: .normal)
//        
//            }
        
            if (self.word.is_owner == 1) {
            
                let urlpath = "https://graph.facebook.com/"+common.getSnsId()+"/picture?type=small"
                let url = NSURL(string:urlpath)! as URL
                isYours.sd_setImage(with:url)

            } else {
            
                let myImage = UIImage(named: "yours")
            
                // 画像をUIImageViewに設定する.
                isYours.image = myImage
            
        
            }
        }
        
//        
//        comment1.sizeToFit()
//        comment2.sizeToFit()
        
        
    }
    
    @IBAction func onRemoveFromMyList(_ sender: Any) {
        
        if (!Common.isInternetAvailable()) {
            
            Common.showAlert(v: self.presenter?.view as! UIViewController,option:0,message:Lang.ERROR_OFFLINE);
            
            //fbログイン中
        } else if Common.isLoggedInWithFacebook() == true {
            
            self.presenter?.removeFromMyListWords(v: word,list_type: (self.presenter?.mylistType)!)
            
            self.presenter?.onRemovedFromMyList(word: word,message:Lang.COMPLETED_REMOVE_FROM_MYLIST_TRANSACTION)
            
        } else {
            Common.showAlert(v: self.presenter?.view as! UIViewController,option:0,message:Lang.ERROR_PLEASE_LOGIN);
        }

    }
    
    

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}


/**
 * データがない場合の表示処理
 */
extension CollectionViewMyListCell {
    
    func displayOffLineView(message:String) {
        
        // UIButtonのインスタンス
        nodataButton = UIButton()
        
        let screenSize: CGRect = UIScreen.main.bounds
        
        // ステータスバーの高さを取得
        let statusBarHeight: Int = Int(UIApplication.shared.statusBarFrame.height)
        
        
        // ボタンのテキスト設定
        nodataButton.setTitle(message, for: .normal)
        
        // ボタンのテキスト色を指定（これがないと白い文字になるため背景と同化して見えない）
        nodataButton.setTitleColor(UIColor.white, for: .normal)
        
        // 大きさの自動調節
        nodataButton.sizeToFit()
        
        let screenWidth = Int(screenSize.width)
        let screenHeight = Int(screenSize.height)
        
        let buttonHeight = (screenHeight/2)-(statusBarHeight + Constants.NAVIGATION_HEIGHT + Int(nodataButton.frame.size.height/2))
        
        let butttonWidth = (screenWidth/2)-(Int(nodataButton.frame.size.width/2))
        var viewFrame:CGRect = nodataButton.frame
        viewFrame.origin = CGPoint(x: butttonWidth, y: buttonHeight)
        // 画像の中心を画面の中心に設定
        nodataButton.frame = viewFrame
        self.addSubview(nodataButton)
        
    }
}



