//
//  DataStore.swift
//  MyUiScrollView
//
//  Created by 竹野学 on 2017/04/18.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift

class EditDataRepository {
    
    var word = Word()
    var usecase: WordUsecaseDelegate
    var common:Common
    
    init(usecase:WordUsecaseDelegate) {
        self.usecase = usecase
        self.common = Common()
        self.common.setUserInfo()
    }
    
    /**
     *
     *サーバーがデータをRequestする
     */
    func editWord(word:Word){
        
        self.word = word
        
        //オンラインの場合
        if (Common.isInternetAvailable()) {
            print("online"+word.word_id.description+":"+word.comment2)
            registerDataFromOnLine()
        } else {
            print("offline")
        }
        
    }
    
    
    private func registerDataFromOnLine () {
        
        let params: [String: Any] = ["word_id":word.word_id,"category":word.category_id,"comment1":word.comment1,"comment2":word.comment2,"user_id":common.getUserId()]
        
        Alamofire.request(Constants.URL_EDIT_WORDS, method: .post, parameters: params, encoding: URLEncoding.httpBody).responseJSON
            {
                response in
                
                self.loadedData(response: response)
                self.usecase.editDone()
        }
        
        
    }
    
    private func getDataFromOffLine () {
        
    }
    
    private func loadedData(response: DataResponse<Any>?){
        
        if let data = response?.data {
            
            let json = JSON(data)
            
            let resultCode = json["ResultCode"].int
            
            if (resultCode == 0) {
                
                var results = json["Results"]
                let dataArg = results["Data"]
//                let word_id:Int = dataArg["word_id"].int!
//                word.word_id = word_id
                
                self.editInRealm()
            }
            
        }
        
    }
    
    
}


/**
 * ローカル側にもデータを登録する
 */
extension EditDataRepository {
    
    func editInRealm() {
        
        let realm = try! Realm()
        
        let filterCon = "id = " + word.word_id.description
        let obj = realm.objects(Myword.self).filter(filterCon)
        
        if obj.count == 0 {
            
            print(5)
            
            /* 書き込み */
            let myword = Myword()
            myword.id  = word.word_id
            myword.comment1  = word.comment1
            myword.comment2  = word.comment2
            myword.user_id = common.getUserId()
            
            try! realm.write() {
                realm.add(myword)
            }
            
        } else {
            //更新
            print(6)
            
            let userObj = obj.first
            try! realm.write {
                userObj!.comment1 = word.comment1
                userObj!.comment2 = word.comment2
                userObj!.user_id = common.getUserId()
                userObj!.auther_user_id = common.getUserId()
                
            }
            
            
//            print((userObj?.user_id.description)!+"::"+common.getUserId().description)
        }

    }
}

//
//
///**
// * ワードリストを取得した後に実行されるコールバックメソッドを実装する。
// */
//extension RegisterViewController: WordViewDelegate {
//
//    func onLoaded(words:[Word]) {
//
////        self.words += words
////        count = self.words.count
////
////        //scrollviewの描画を行う
////        setScrollView()
//
//    }
//
//}

