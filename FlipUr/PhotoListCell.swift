//
//  PhotoListCell.swift
//  pinterest-like-ui
//
//  Created by Eiji Kushida on 2016/12/02.
//  Copyright © 2016年 Eiji Kushida. All rights reserved.
//

import UIKit

final class PhotoListCell: UICollectionViewCell {
    
//    @IBOutlet weak var thumnailImageView: UIImageView!
//    @IBOutlet weak var captionLabel: UILabel!
//    @IBOutlet weak var commentLabel: UILabel!
//    @IBOutlet weak var imageViewHeightLayoutConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var imageViewHeightLayoutConstraint: NSLayoutConstraint!
    
//    
    @IBOutlet weak var thumnailImageView: UIImageView!
//    @IBOutlet weak var captionLabel: UILabel!
    
    @IBOutlet weak var comment1Label: UILabel!
    @IBOutlet weak var comment2Label: UILabel!
    var word = Word()
    
    
    static var identifier: String {
        get {
            return String(describing: self)
        }
    }
    
    func setCell(word: Word) {
        
//        thumnailImageView.image = photo.image
        comment1Label.text = word.comment1
        comment2Label.text = word.comment2

        if (word.image_path != "") {
            
            let imageURL = URL(string: word.image_path)
//            thumnailImageView.sd_setImage(with: imageURL)
            let cellImage = UIImage(named: "background2")
            
        } else {
            
            let cellImage = UIImage(named: "background2")
            thumnailImageView.image = cellImage
            
            
            
        }

    }
//    var word: Word? {
//
//        didSet {
//            
//            if let word = word {
////                thumnailImageView.image = photo.image
//                comment1Label.text = word.comment2
//                comment2Label.text = word.comment1
//            }
//        }
//    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        thumnailImageView.image = nil
        comment1Label.text = ""
        comment2Label.text = ""
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        
        if let attributes = layoutAttributes as? PinterestLayoutAttributes {
            imageViewHeightLayoutConstraint.constant = attributes.photoHeight
        }
    }
}
