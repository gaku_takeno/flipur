//
//  PhotoRepository.swift
//  MyUiScrollView
//
//  Created by 竹野学 on 2017/04/18.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation

protocol WordUsecaseDelegate {
    func loadedMyList(is_last:Int,words:[Word])
    func done(is_last:Int,words:[Word])
    func reloadDone(is_last:Int,words:[Word])
    func registerDone()
    func editDone()
    func deleteDone()
    func bookMarkDone(message:String)
    func rememberDone(message:String)
    func removeFromMyListDone(message:String)
}
