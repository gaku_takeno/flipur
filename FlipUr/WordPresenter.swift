//
//  PhotoListPresenter.swift
//  MyUiScrollView
//
//  Created by 竹野学 on 2017/04/18.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation


final class WordPresenter {
    
    var usecase: WordUsecase?
    var view:WordViewDelegate
    var mylistType = 0
    var pageIndex:Int = 0
    
    init(view: WordViewDelegate) {
        self.view = view
        
        usecase = WordUsecase(presenter: self)
    }
    
    
    
    /**
     * Reload All LIST 読み込み開始
     */
    func reloadAllList() {
        
        usecase?.getPhotos(is_reload:1)
    }
    
    
    /**
    * All LIST 読み込み開始
    */
    func loadPhotos() {
        
        usecase?.getPhotos(is_reload:0)
    }
    
    /**
     * ALL LIST 読み込み完了
     */
    func loaded(words:[Word],is_last:Int) {
        self.view.onLoaded(words: words,is_last: is_last)
    }
    
    /**
     * ALL LIST 読み込み完了
     */
    func reloaded(words:[Word],is_last:Int) {
        self.view.onReloaded(words: words)
    }

    /**
    * My list typeを設定する
    */
    func setMyListType(type:Int){
        self.mylistType = type
    }
    
    /**
     * MY LIST 読み込み開始
     */
    func loadMyList(type:Int) {
        
        usecase?.getMyList(type:type)
    }
    
    /**
     * MY LIST 読み込み完了
     */
    func loadedMyList(words:[Word]) {
        self.view.onLoaded(words: words,is_last: 0)
    }

    
    
    /**
     * 登録開始
     */
    func registerWords(v:Word) {
        
        usecase?.registerWords(word:v)
    }
    
    /**
     * 登録完了
     */
    func registered(word:Word) {
        self.view.onRegistered(word: word)
    }
    
    /**
     * 削除開始
     */
    func deleteWords(v:Word,pageIndex:Int) {
        
        self.pageIndex = pageIndex
        usecase?.deleteWords(word:v)
    }
    
    /**
     * 削除完了
     */
    func onDeleted(word:Word) {
        self.view.onDeleted(pageIndex: self.pageIndex, word: word)
    }
    
    
    
    /**
     * 編集開始
     */
    func editWords(v:Word) {
        
        usecase?.editWords(word:v)
    }
    
    /**
     * 編集完了
     */
    func onEdited(word:Word) {
        self.view.onEdited(word: word)
    }
    
    
    
    /**
     * ブックマーク開始
     */
    func bookMarkWords(v:Word) {
        
        usecase?.bookMarkWords(word:v)
    }
    
    /**
     * ブックマーク完了
     */
    func onBookMarked(word:Word,message:String) {
        self.view.onBookMarked(word: word,message:message)
    }
    
    
    /**
     * リメンバー開始
     */
    func rememberWords(v:Word) {
        
        usecase?.rememberWords(word:v)
    }
    
    /**
     * リメンバー完了
     */
    func onRemembered(word:Word,message:String) {
        self.view.onRemembered(word: word,message:message)
    }
    
    
    
    /**
     * remove From MyList 開始
     */
    func removeFromMyListWords(v:Word,list_type:Int) {
        
        usecase?.removeFromMyListWords(word:v,list_type:list_type)
    }
    
    /**
     * remove From MyList 完了
     */
    func onRemovedFromMyList(word:Word,message:String) {
        print("onRemovedFromMyList Completed!!")
        self.view.onRemovedFromMyList(word: word,message:message)
    }
    
    
    
    
}
