//
//  Photo.swift
//  MyUiScrollView
//
//  Created by 竹野学 on 2017/04/18.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation

class Word: NSObject {
    
    var is_no_data = 0
    var indexPath:IndexPath = []
    var word_id:Int = 0
    var comment1  = ""
    var comment2  = ""
    var category_id:Int = 0
    var is_owner:Int = 0
    var is_bookmarked:Int = 0
    var is_remembered:Int = 0
    var base64StringImageData = ""
    var image_path = ""
    var updated_at:String = ""
    
    func heightForCaption(_ font: UIFont, width: CGFloat) -> CGFloat {
        
        let rect = NSString(string: comment1).boundingRect(
            with: CGSize(width: width, height: CGFloat(MAXFLOAT)),
            options: .usesLineFragmentOrigin,
            attributes: [NSFontAttributeName: font], context: nil)
        
        return ceil(rect.height)
    }
    
    func heightForComment(_ font: UIFont, width: CGFloat) -> CGFloat {
        
        let rect = NSString(string: comment2).boundingRect(
            with: CGSize(width: width, height: CGFloat(MAXFLOAT)),
            options: .usesLineFragmentOrigin,
            attributes: [NSFontAttributeName: font], context: nil)
        
        return ceil(rect.height)
    }

    
}
