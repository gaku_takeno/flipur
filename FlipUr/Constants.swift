//
//  Constants.swift
//  MyUiScrollView
//
//  Created by 竹野学 on 2017/04/19.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation

class Constants {
    

    static let URL_API : String = "https://deca-nofiction.ssl-lolipop.jp/my_lang/"
    static let URL_GET_WORDS : String = URL_API+"get_words_lang.php"
    static let URL_GET_MY_LIST_WORDS : String = URL_API+"get_mylist_words_lang.php"
    static let URL_REGIST_WORDS : String = URL_API+"regist_words_lang.php"
    static let URL_EDIT_WORDS : String = URL_API+"edit_words_lang.php"
    static let URL_DELETE_WORDS : String = URL_API+"delete_words_lang.php"
    static let URL_REGIST_USERS : String = URL_API+"regist_users_lang.php"
    static let URL_BOOKMARK_WORDS_USERS : String = URL_API+"bookmark_word_lang.php"
    static let URL_REMEMBER_WORDS_USERS : String = URL_API+"remember_word_lang.php"
    
    static let URL_REPORT_WORDS_USERS : String = URL_API+"report_word_lang.php"
    static let URL_BLOCK_WORDS_USERS : String = URL_API+"block_word_lang.php"
    
    static let URL_REMOVE_FROM_MYLIST : String = URL_API+"remove_mylist_lang.php"
    
    static let URL_MAIN_DEFAULT_IMAGE_PATH : String = URL_API+"image/main.jpg"
    static let URL_BOOKMARK_DEFAULT_IMAGE_PATH : String = URL_API+"image/bookmark.jpg"
    static let URL_REMEMBER_DEFAULT_IMAGE_PATH : String = URL_API+"image/remember.jpg"
    
    
    static let URL_APPLE_SHARE = "https://www.apple.com/jp/watch/"
    
    
//    static let TITLE_APPLE_SHARE = "Apple - Apple Watch"
    
    static let DIALOG_TIME_CLOSING :Double = 1.2
    
    // ステータスバーの高さを取得
    static let STATUS_BAR_HEIGHT: CGFloat = UIApplication.shared.statusBarFrame.height
    
    // ナビゲーションバーの高さを取得
//    let navBarHeight = self.navigationController?.navigationBar.frame.size.height

    static let NAVIGATION_HEIGHT :Int = 44 + Int(UIApplication.shared.statusBarFrame.height)
    
    static let MENU_BAR_HEIGHT :Int = 44
    
    
    static let CACHE_SEC : TimeInterval = 5 * 60; //5分キャッシュ
    
    static let MYLIST_TYPE_NORMAL:Int = 0
    static let MYLIST_TYPE_BOOKMARK:Int = 1
    static let MYLIST_TYPE_REMEMBER:Int = 2
    
    static let MYLIST_MAX_ITEM_NUMBER:Int = 5
}
