//
//  DataStore.swift
//  MyUiScrollView
//
//  Created by 竹野学 on 2017/04/18.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift

class SettingReportDataRepository {
    
    var word = Word()
    var usecase: SettingUsecaseDelegate
    var common:Common
    
    init(usecase:SettingUsecaseDelegate) {
        self.usecase = usecase
        self.common = Common()
        self.common.setUserInfo()
    }
    
    func reportDataFromOnLine (word:Word) {
        
        self.word = word
        let params: [String: Any] = ["word_id":word.word_id,"user_id":common.getUserId()]
        
        Alamofire.request(Constants.URL_REPORT_WORDS_USERS, method: .post, parameters: params, encoding: URLEncoding.httpBody).responseJSON
            {
                response in
                self.reportedData(response: response)
                
        }
        
        
    }
    
    
    private func reportedData(response: DataResponse<Any>?){
        
        if let data = response?.data {
            
            let json = JSON(data)
            
            let resultCode = json["ResultCode"].int
            
            if (resultCode == 0) {
                
                self.deleteInRealm()
                self.usecase.reportDone(message:Lang.COMPLETED_REPORT_TRANSACTION)
            }
            
        }
        
    }
    
    
}


/**
 * ローカル側にもデータを登録する
 */
extension SettingReportDataRepository {
    
    func deleteInRealm() {
        
        let realm = try! Realm()
        
        let filterCon = "id = " + word.word_id.description
        let obj = realm.objects(Myword.self).filter(filterCon)
        
        if obj.count > 0 {
            
            try! realm.write() {
                realm.delete(obj)
                print("削除:delete")
            }
            
            let objects = realm.objects(Myword.self)
            print(objects.count.description+"個になりました。")
        }
        
        
        
    }
}
