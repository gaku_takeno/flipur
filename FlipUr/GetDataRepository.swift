//
//  DataStore.swift
//  MyUiScrollView
//
//  Created by 竹野学 on 2017/04/18.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift

class GetDataRepository {

    var usecase: WordUsecaseDelegate
    var common:Common
    
    init(usecase:WordUsecaseDelegate) {
        self.usecase = usecase
        self.common = Common()
        self.common.setUserInfo()
        
    }
    
    
    func getDataFromOnLine (is_reload:Int) {
        
        let category = 1
        let params: [String: Any] = ["category":category,"user_id":common.getUserId()]
        Alamofire.request(Constants.URL_GET_WORDS, method: .post, parameters: params, encoding: URLEncoding.httpBody).responseJSON
            {
                response in
                    self.loadedData(response: response,is_reload:is_reload)
        }
    }
    
    
    
    func getMyListDataFromOnLine (type:Int) {
        
        let category = 1
        let params: [String: Any] = ["category":category,"user_id":common.getUserId(),"list_type":type]
        
        Alamofire.request(Constants.URL_GET_MY_LIST_WORDS, method: .post, parameters: params, encoding: URLEncoding.httpBody).responseJSON
            {
                response in
                
                self.loadedDataMyList(response: response)
                
        }
        
        
    }
    
    
    /**
    * OFF LINE時/Logout時
    */
    func getDataFromOffLine () {
        
        var words = [Word]()
        let word = Word()

        words.append(word)
        self.usecase.done(is_last:1,words:words)
        
    }
    
    /**
     * realmからMy Listを読み込み
     */
    func getMyListDataFromOffLine (type:Int) {
     
        
            
        let realm = try! Realm()
        
        let mywords:Results<Myword>!
        
        //bookmark/remember両方
        if (type == 1) {
            //bookmark
            mywords = realm.objects(Myword.self).filter("is_bookmarked = 1")
            
        } else if (type == 2) {
            //remember
            mywords = realm.objects(Myword.self).filter("is_remembered = 1")
            
        } else {
            mywords = realm.objects(Myword.self)
            
        }
        
        var words = [Word]()
        print("offline mylist")
        print(mywords.count.description)
        for myword in mywords {
                
            let word = Word()
                
            word.comment1 =  myword.comment1
            word.comment2 =  myword.comment2
            word.category_id = myword.category_id
            word.word_id = myword.id
            word.is_owner =  myword.is_owner
            word.is_bookmarked =  myword.is_bookmarked
            word.is_remembered =  myword.is_remembered
            word.image_path = myword.image_path
            word.updated_at = ""
            
            words.append(word)
                
        }
        
        var is_last = 0
        if (mywords.count <= Constants.MYLIST_MAX_ITEM_NUMBER) {
            is_last = 1
        }
        self.usecase.loadedMyList(is_last:is_last,words:words)
        
    }
    
    
    private func loadedData(response: DataResponse<Any>?,is_reload:Int){
        
        if let data = response?.data {
            
            
            var words = [Word]()
            let json = JSON(data)
            
            let resultCode = json["ResultCode"].int
            
            if (resultCode == 0) {
                var results = json["Results"]
                let dataArg = results["Data"]
                let info = dataArg
            
                info.forEach{(_, _data) in
                    
                    let word = Word()
                    word.comment1 =  _data["comment1"].string!
                    word.comment2 =  _data["comment2"].string!
                    if let image_path = _data["image_path"].string {
                        word.image_path = Constants.URL_API + "image/" + common.getUserId().description + "/" + image_path
                    }
                    
                    word.category_id =  _data["category_id"].intValue
                    word.word_id =  _data["id"].intValue
                    word.is_owner =  _data["is_owner"].intValue
                    word.is_bookmarked =  _data["is_bookmarked"].intValue
                    word.is_remembered =  _data["is_remembered"].intValue
                    word.updated_at =  _data["updated_at"].string!
                    
                    words.append(word)
                    
                }
             
                if (is_reload == 1) {
                    self.usecase.reloadDone(is_last:0,words:words)
                } else {
                    
                    self.usecase.done(is_last:0,words:words)
                }
                
            }
            
        }
        
    }

    
    private func loadedDataMyList(response: DataResponse<Any>?){
        
        if let data = response?.data {
            var words = [Word]()
            let json = JSON(data)
            
            let resultCode = json["ResultCode"].int
            
            if let is_last = json["Is_last"].int {
                
                if (resultCode == 0) {
                    
                    var results = json["Results"]
                    let dataArg = results["Data"]
                    let info = dataArg
                    
                    info.forEach{(_, _data) in
                        
                        let word = Word()
                        word.comment1 =  _data["comment1"].string!
                        word.comment2 =  _data["comment2"].string!
                        
                        
                        if let image_path = _data["image_path"].string {
                            word.image_path = Constants.URL_API + "image/" + common.getUserId().description + "/" + image_path
                            
                        }
                        word.category_id =  _data["category_id"].intValue
                        word.word_id =  _data["id"].intValue
                        word.is_owner =  _data["is_owner"].intValue
                        word.is_bookmarked =  _data["is_bookmarked"].intValue
                        word.is_remembered =  _data["is_remembered"].intValue
                        word.updated_at =  _data["updated_at"].string!
                        
                        
                        words.append(word)
                        
                    }
                    
                    self.usecase.loadedMyList(is_last:is_last,words:words)
                }
            }
        }
        
    }
    

    
}


//extension GetDataRepository {
//    
//    
//    
//    //    /**
//    //     * GETリクエスト
//    //     */
//    //    func loadImageGet () {
//    //
//    //        Alamofire.request("http://49.212.187.143/api.json", parameters: ["date": "20161013"]).responseJSON
//    //            { response in
//    //                let json = JSON(response.result.value)
//    //                json["info"].forEach{(_, data) in
//    //                    let type = data["type"].string!
//    //                    let url = data["url"].string!
//    //                    //                print(type+":"+url) // foo or bar
//    //                }
//    //            }
//    //    }
//    //    
//    
//
//}
