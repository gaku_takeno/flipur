//
//  SettingUsecaseDelegate.swift
//  FlipUr
//
//  Created by 竹野学 on 2017/06/20.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation

protocol SettingUsecaseDelegate {
    func reportDone(message:String)
    func blockDone(message:String)
    
}
