import UIKit

extension UIColor {
    
    //hexは6桁の16進数文字列（例：FF001A）、alphaは透明度0〜1
    convenience init(hex: String, alpha: CGFloat) {
        
        //文字列を2桁ずつに分ける処理
        //文字列を区切るインデックスを設定
        let r = hex.startIndex;
        let g = hex.index(r, offsetBy: 2);
        let b = hex.index(g, offsetBy: 2);
        let e = hex.index(b, offsetBy: 2);
        
        //R,G,B用に文字列を2桁ずつ3つに分解
        let hexR:String = hex.substring(with: r ..< g);
        let hexG:String = hex.substring(with: g ..< b);
        let hexB:String = hex.substring(with: b ..< e);
        
        //RGB値は0〜255なので、変換する
        let R255:CGFloat = CGFloat(Int(hexR, radix: 16) ?? 0)/255;
        let G255:CGFloat = CGFloat(Int(hexG, radix: 16) ?? 0)/255;
        let B255:CGFloat = CGFloat(Int(hexB, radix: 16) ?? 0)/255;
        
        //UIColorをRGBで指定するinitを呼ぶ
        self.init(red: R255, green: G255, blue: B255, alpha: alpha);
    }
    
    //alphaを指定しないとき(不透明度100%)
    convenience init(hex: String) {
        
        //alphaを1にして上のinitを呼ぶ
        self.init(hex: hex, alpha: 1.0);
    }
}
