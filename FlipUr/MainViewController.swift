//
//  MainViewController.swift
//  MyLang
//
//  Created by 竹野学 on 2017/05/09.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import UIKit
import RealmSwift

class MainViewController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout,UIScrollViewDelegate {


    var delegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    var selectedImage: UIImage?
    var presenter: WordPresenter?
    var settingPresenter: SettingPresenter?
    var common:Common!
    var words = [Word]()
    var count = 0
    var loadingObj:ShowLoadingBar?
    
    override func viewDidAppear(_ animated: Bool) {
        
        if (Common.getMainStatusChange() == 1) {
//            print("viewDidAppear-getMainStatusChange")
            presenter?.loadPhotos()
            Common.setMainStatusChange(val:0)
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let config = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
        Realm.Configuration.defaultConfiguration = config
        
        // DBファイルのfileURLを取得
//        if let fileURL = Realm.Configuration.defaultConfiguration.fileURL {
//            try! FileManager.default.removeItem(at: fileURL)
//        }

        common = Common()
        common.setScreenSize()
    
        presenter = WordPresenter(view: self)
        presenter?.loadPhotos()
        
        settingPresenter = SettingPresenter(view: self)
        
        //collectionViewの高さを調整する。これをしないとセル一個一個の高さが全体的にずれる。
        collectionView.frame.size.height = common.getHeight() - CGFloat(Constants.NAVIGATION_HEIGHT) - CGFloat(Constants.MENU_BAR_HEIGHT)
        collectionView.frame.size.width = common.getWidth()
        
        loadingObj = ShowLoadingBar(view:self.view)
        loadingObj?.setLoadingBar()
        if (Common.isInternetAvailable()) {
            loadingObj?.ActivityIndicator.startAnimating()
        }
        
        
        //リフレッシュコントロールを作成する。
        setRefreshCtr()
    }
    
  
    
    //unwind
    @IBAction func exitToMain(segue: UIStoryboardSegue) {
//        if (segue.identifier == "back") {
            print("back!")
//        }
        
    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        // Cell はストーリーボードで設定したセルのID
        let collectionCell:CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionViewCell
        
    
        let word = words[(indexPath as NSIndexPath).row]
        word.indexPath = indexPath

//        collectionCell.setUiView(v: self)
        collectionCell.setCell(word: words[(indexPath as NSIndexPath).row],presenter:presenter!,settingPresenter:settingPresenter!)
        
        return collectionCell
    }
    
    
    // Cell が選択された場合
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if selectedImage != nil {
            // SubViewController へ遷移するために Segue を呼び出す
//            performSegue(withIdentifier: "toSubViewController",sender: nil)
        }
        
    }
    
    // Screenサイズに応じたセルサイズを返す
    // UICollectionViewDelegateFlowLayoutの設定が必要
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let cellSizeW:CGFloat = common.getWidth()
//        let cellSizeH:CGFloat = common.getHeight()
        let cellSizeH:CGFloat = common.getHeight() - CGFloat(Constants.NAVIGATION_HEIGHT) - CGFloat(Constants.MENU_BAR_HEIGHT)
        
        
        // 正方形で返すためにwidth,heightを同じにする
        return CGSize(width: cellSizeW, height: cellSizeH)
    }
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if (Common.isInternetAvailable() && indexPath.row == self.count - 2) {
            presenter?.loadPhotos()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // 要素数を入れる、要素以上の数字を入れると表示でエラーとなる
        return words.count;
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}


/**
 * ワードリストを取得した後に実行されるコールバックメソッドを実装する。
 */
extension MainViewController: WordViewDelegate {
    
    func onLoaded(words:[Word],is_last: Int) {
        
        self.words += words
        count = self.words.count
        
        //scrollviewの描画を行う
        setScrollView()
        
        // クルクルストップ
        loadingObj?.ActivityIndicator.stopAnimating()
        
        
    }
    
    func onReloaded(words:[Word]) {
        
        self.words = words
        count = self.words.count
        //scrollviewの描画を行う
        setScrollView()
        
    }
    
    func onRegistered(word:Word) {}
    func onEdited(word:Word){}
    func onDeleted(pageIndex:Int,word: Word) {}

    func onBookMarked(word:Word,message:String){
        common.showAlert(v: self, option: 0, message: message)
        delegate.bookmarkViewController?.presenter?.loadMyList(type:Constants.MYLIST_TYPE_BOOKMARK)
    }
    
    func onRemembered(word:Word,message:String){
        common.showAlert(v: self, option: 0, message: message)
        delegate.rememberViewController?.presenter?.loadMyList(type:Constants.MYLIST_TYPE_REMEMBER)
    }
    
    func onRemovedFromMyList(word: Word, message: String) {}
    
}

extension MainViewController:SettingViewDelegate {
    
    func onBlocked (word:Word,message:String) {
        
        let index = (word.indexPath as NSIndexPath).row
        self.words.remove(at: index)
        
        count = self.words.count
        
        if (count < 1) {
            let word = Word()
            word.is_no_data = 1
            self.words.append(word)
        }
        
        //scrollviewの描画を行う
        setScrollView()
        
        common.showAlert(v: self, option: 0, message: message)
    }
    
    func onReported (word:Word,message:String) {
        
        let index = (word.indexPath as NSIndexPath).row
        self.words.remove(at: index)
        
        count = self.words.count
        
        if (count < 1) {
            let word = Word()
            word.is_no_data = 1
            self.words.append(word)
        }
        
        //scrollviewの描画を行う
        setScrollView()
        
        common.showAlert(v: self, option: 0, message: message)
    }
}



/**
 * scrollviewの描画を行う。
 */
extension MainViewController {
    
    func setScrollView() {
        collectionView.reloadData()
    }
    
    
}


/**
 * リフレッシュコントロールを作成する。
 */
extension MainViewController {
    
    func setRefreshCtr () {
        let refresh = UIRefreshControl()
        
        //インジケーターの下に表示する文字列を設定する。
        refresh.attributedTitle = NSAttributedString(string: "loading...")
        
        //インジケーターの色を設定する。
        refresh.tintColor = UIColor.blue
        
        //テーブルビューを引っ張ったときの呼び出しメソッドを登録する。
//        refresh.addTarget(self, action: Selector("startLoading:"), for: UIControlEvents.valueChanged)
        refresh.addTarget(self, action: Selector("startLoading:"), for: UIControlEvents.valueChanged)
        
        //ビューコントローラーのプロパティにリフレッシュコントロールを設定する。
        collectionView.refreshControl = refresh
        
        collectionView?.addSubview(refresh)
        
    }
    
    //MARK: RefreshControl
    func startLoading(refreshControl : UIRefreshControl) {
        // 別スレッドで2秒処理したことにしてみる
//        Thread.detachNewThreadSelector(Selector("wait:"), toTarget: self, with: refreshControl)
        sleep(4)
            print("refreshed!!")
    }
    
    func wait(refreshControl : UIRefreshControl) {
        // 2 sec
        sleep(4)
        DispatchQueue.main.async() {
            refreshControl.endRefreshing()
            print("refreshed!!")
        }
    }
    
}
