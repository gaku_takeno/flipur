//
//  showLoadingBar.swift
//  FlipUr
//
//  Created by 竹野学 on 2017/07/05.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation

class ShowLoadingBar {

    var ActivityIndicator: UIActivityIndicatorView!
    
    var v:UIView
    
    init(view:UIView) {
        self.v = view
//        setLoadingBar()
    }
    
    
    func  setLoading() {
        
        // ActivityIndicatorを作成＆中央に配置
        ActivityIndicator = UIActivityIndicatorView()
        
        ActivityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        ActivityIndicator.center = self.v.center
        
        // クルクルをストップした時に非表示する
        ActivityIndicator.hidesWhenStopped = true
        
        // 色を設定
        ActivityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        //Viewに追加
        self.v.addSubview(ActivityIndicator)
    }
    
    func  setLoadingBar() {
        
        let screenSize: CGRect = UIScreen.main.bounds
        // ステータスバーの高さを取得
        let statusBarHeight: Int = Int(UIApplication.shared.statusBarFrame.height)

        let screenWidth = Int(screenSize.width)
        let screenHeight = Int(screenSize.height)

        
        // ActivityIndicatorを作成＆中央に配置
        ActivityIndicator = UIActivityIndicatorView()
        
//        ActivityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
//        ActivityIndicator.center = self.v.center
        
        let buttonHeight = (screenHeight/2)-(statusBarHeight + Constants.NAVIGATION_HEIGHT + Int(ActivityIndicator.frame.size.height/2))
        
        let butttonWidth = (screenWidth/2)-(Int(ActivityIndicator.frame.size.width/2))

        var viewFrame:CGRect = ActivityIndicator.frame
        viewFrame.origin = CGPoint(x: butttonWidth, y: buttonHeight)
        // 画像の中心を画面の中心に設定
        ActivityIndicator.frame = viewFrame
        
        
        
    
        // クルクルをストップした時に非表示する
        ActivityIndicator.hidesWhenStopped = true
    
        // 色を設定
        ActivityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
    
        //Viewに追加
        self.v.addSubview(ActivityIndicator)
    }
}
