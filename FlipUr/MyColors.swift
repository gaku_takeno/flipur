//
//  MyColors.swift
//  MyLang
//
//  Created by 竹野学 on 2017/05/15.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation
class MyColors {
    
    static let SKY_BLUE = UIColor(hex: "00BFFF")
    static let MAIN_COLOR = UIColor(hex: "e0c76f")
    
}
