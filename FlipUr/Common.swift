//
//  Common.swift
//  MyUiScrollView
//
//  Created by 竹野学 on 2017/04/18.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import RealmSwift
import FacebookCore
import FacebookLogin
//import FBSDKCoreKit

class Common {
    
    private var screenWidth:CGFloat = 0
    private var screenHeight:CGFloat = 0
    private var name:String = ""
    private var snsid:String = ""
    private var user_id:Int = 0
    
    
    static func setIconStatusChange (val:Int) {
        
        // UserDefaults のインスタンス
        let userDefaults = UserDefaults.standard
        // Keyを指定して保存
        userDefaults.set(val, forKey: "is_icon_reload_flg")

//        userDefaults.set(val, forKey: "is_remember_reload_flg")
        
        userDefaults.synchronize()
        
    }
    
    
    static func getIconStatusChange () -> Int {
        
        // UserDefaults のインスタンス
        let userDefaults = UserDefaults.standard
        let is_login: Int = userDefaults.object(forKey: "is_icon_reload_flg") as! Int
        
        return is_login
        
    }
    
    
    static func setMainStatusChange (val:Int) {
        
        // UserDefaults のインスタンス
        let userDefaults = UserDefaults.standard
        // Keyを指定して保存
        userDefaults.set(val, forKey: "is_main_reload_flg")
        
        //        userDefaults.set(val, forKey: "is_remember_reload_flg")
        
        userDefaults.synchronize()
        
    }
    
    
    static func getMainStatusChange () -> Int {
        
        // UserDefaults のインスタンス
        let userDefaults = UserDefaults.standard
        let is_login: Int = userDefaults.object(forKey: "is_main_reload_flg") as! Int
        
        return is_login
        
    }
    
    static func setBookmarkStatusChange (val:Int) {
        
        // UserDefaults のインスタンス
        let userDefaults = UserDefaults.standard
        // Keyを指定して保存
        userDefaults.set(val, forKey: "is_bookmark_reload_flg")
        userDefaults.synchronize()
        
    }
    
    
    static func getBookmarkStatusChange () -> Int {
        
        // UserDefaults のインスタンス
        let userDefaults = UserDefaults.standard
        let is_login: Int = userDefaults.object(forKey: "is_bookmark_reload_flg") as! Int
        
        return is_login
        
    }

    
    static func setRememberStatusChange (val:Int) {
        
        // UserDefaults のインスタンス
        let userDefaults = UserDefaults.standard
        // Keyを指定して保存
        userDefaults.set(val, forKey: "is_remember_reload_flg")
        
        userDefaults.synchronize()
        
    }
    
    
    static func getRememberStatusChange () -> Int {
        
        // UserDefaults のインスタンス
        let userDefaults = UserDefaults.standard
        let is_login: Int = userDefaults.object(forKey: "is_remember_reload_flg") as! Int
        
        return is_login
        
    }



    
    func setUserInfo() {
        
        let realm = try! Realm()
        let obj = realm.objects(User.self)
        if obj.count > 0 {

            let inf = obj.first
            self.name = (inf?.name)!
            self.user_id = (inf?.id)!
            self.snsid = (inf?.snsid)!
        }
    }
    
    func getUserId () -> Int{
        return self.user_id
    }
    
    func getSnsId () -> String{
        return self.snsid
    }
    
    func getUserName () -> String{
        return self.name
    }
    
    /**
     *   画面の横幅を取得
     */
    func setScreenSize() {
        let screenSize: CGRect = UIScreen.main.bounds
        
        self.screenWidth = screenSize.width
        self.screenHeight = screenSize.height
        
    }
    
    func getWidth () -> CGFloat{
        return self.screenWidth
    }
    
    func getHeight () -> CGFloat{
        return self.screenHeight
    }
    
    
    static func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    
    
    func showAlert(v: UIViewController,option:Int,message:String){
        
        // アラート作成
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        // アラート表示
        v.present(alert, animated: true, completion: {
            // アラートを閉じる
            DispatchQueue.main.asyncAfter(deadline: .now() + Constants.DIALOG_TIME_CLOSING, execute: {
                alert.dismiss(animated: true, completion: nil)
//                function
                if (option == 1) {
                    v.dismiss(animated: true, completion: nil)
                }
            })
        })
    }
    
    
    static func showAlert(v: UIViewController,option:Int,message:String){
        
        // アラート作成
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        // アラート表示
        v.present(alert, animated: true, completion: {
            // アラートを閉じる
            DispatchQueue.main.asyncAfter(deadline: .now() + Constants.DIALOG_TIME_CLOSING, execute: {
                alert.dismiss(animated: true, completion: nil)
                //                function
                if (option == 1) {
                    v.dismiss(animated: true, completion: nil)
                }
            })
        })
    }
    

    
    
    /**
     * FBログイン状態をチェックする
     */
    static func isLoggedInWithFacebook() -> Bool {
        let loggedIn = AccessToken.current != nil
        return loggedIn
    }
    
    static func matchesForRegexInText(regex: String, text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matches(in: text,
                                        options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch let error as NSError {
            return []
        }
    }

    
    
    /**
     * 画像のリサイズ
     */
    static func cropThumbnailImage(image :UIImage, w:Double, h:Double) ->UIImage
    {
        // リサイズ処理
        let origRef    = image.cgImage
        let origWidth  = Double(origRef!.width)
        let origHeight = Double(origRef!.height)
        var resizeWidth:Double = 0, resizeHeight:Double = 0
        
        if (origWidth < origHeight) {
            resizeWidth = w
            resizeHeight = origHeight * resizeWidth / origWidth
        } else {
            resizeHeight = h
            resizeWidth = origWidth * resizeHeight / origHeight
        }
        
        let resizeSize = CGSize.init(width: CGFloat(resizeWidth), height: CGFloat(resizeHeight))
        
        UIGraphicsBeginImageContext(resizeSize)
        
        image.draw(in: CGRect.init(x: 0, y: 0, width: CGFloat(resizeWidth), height: CGFloat(resizeHeight)))
        
        let resizeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        // 切り抜き処理
        
        let cropRect  = CGRect.init(x: CGFloat((resizeWidth - w) / 2), y: CGFloat((resizeHeight - h) / 2), width: CGFloat(w), height: CGFloat(h))
        let cropRef   = resizeImage!.cgImage!.cropping(to: cropRect)
        let cropImage = UIImage(cgImage: cropRef!)
        
        return cropImage
    }

    
    
//    
//    /**
//     *
//     *与えられた画像を暗くする。
//     * image: 元画像
//     * - level: 暗くするレベル 0-1
//     * Returns: 加工後画像
//    */
//    func darken(image:UIImage, level:CGFloat) -> UIImage{
//        
//        // 一時的な暗くするようの黒レイヤ
//        let frame = CGRect(origin:CGPoint(x:0,y:0),size:image.size)
//        let tempView = UIView(frame:frame)
//        tempView.backgroundColor = UIColor.black
//        tempView.alpha = level
//        
//        // 画像を新しいコンテキストに描画する
//        UIGraphicsBeginImageContext(frame.size)
//        let context = UIGraphicsGetCurrentContext()
//        image.draw(in: frame)
//        
//        // コンテキストに黒レイヤを乗せてレンダー
//        context!.translateBy(x: 0, y: frame.size.height)
//        context!.scaleBy(x: 1.0, y: -1.0)
//        context!.clip(to: frame, mask: image.cgImage!)
//        tempView.layer.render(in: context!)
//        
//        let imageRef = context!.makeImage()
//        let toReturn = UIImage(cgImage:imageRef!)
//        UIGraphicsEndImageContext()
//        return toReturn
//        
//    }
//
    
}
