
//import Foundation
import RealmSwift
class Myword: Object {
    dynamic var id  = 0
    
    //my_custom_words_list_langテーブルのレコーダの作成者( wordを新規登録したuser_id,wordをbookmarkまたはrememberしたuser_id)
    dynamic var auther_user_id  = 0
    
    //my_words_list_langテーブルのレコーダの作成者(wordを新規登録したオリジナルユーザーのuser_id)
    dynamic var user_id  = 0
    dynamic var comment1 = ""
    dynamic var comment2  = ""
    dynamic var category_id = 1
//    dynamic var type = 0 //0:own 1:bookmark 2:remember
    dynamic var is_owner:Int = 0
    dynamic var is_bookmarked:Int = 0
    dynamic var is_remembered:Int = 0
    dynamic var image_path  = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }

}
