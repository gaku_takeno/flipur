//
//  DataStore.swift
//  MyUiScrollView
//
//  Created by 竹野学 on 2017/04/18.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift

class RegisterDataRepository {
    
    var word = Word()
    var usecase: WordUsecaseDelegate
    var common:Common
    
    init(usecase:WordUsecaseDelegate) {
        self.usecase = usecase
        self.common = Common()
        self.common.setUserInfo()
    }
    
    /**
     *
     *サーバーがデータをRequestする
     */
    func registerWord(word:Word){
        
//        result = word
        self.word = word
        
        //オンラインの場合
        if (Common.isInternetAvailable()) {
            registerDataFromOnLine()
        }
        
    }
    
    
    private func registerDataFromOnLine () {
        
        
        //アップロード写真の回転値
        let orientation:Int = 0
        let params: [String: Any] = ["category":word.category_id,"comment1":word.comment1,"comment2":word.comment2,"user_id":common.getUserId(),"pointimage":word.base64StringImageData,"orientation":orientation]
        
        Alamofire.request(Constants.URL_REGIST_WORDS, method: .post, parameters: params, encoding: URLEncoding.httpBody).responseJSON
            {
                response in
                
                self.loadedData(response: response)
                self.usecase.registerDone()
                
        }
        
        
    }
    
    private func getDataFromOffLine () {
        
    }
    
    private func loadedData(response: DataResponse<Any>?){
        
        word.comment2 = (response?.description)!
        
        if let data = response?.data {
            
            let json = JSON(data)
            
            let resultCode = json["ResultCode"].int
            
            if (resultCode == 0) {
                
                var results = json["Results"]
                let dataArg = results["Data"]
                let word_id:Int = dataArg["word_id"].int!
                word.image_path = dataArg["image_path"].string!
                word.word_id = word_id
                word.base64StringImageData = ""
                self.registerInRealm()
            }
            
        }
        
    }
    
    
}


/**
 * ローカル側にもデータを登録する
 */
extension RegisterDataRepository {
    
    func registerInRealm() {
        
        
        let realm = try! Realm()
        
        let filterCon = "id = " + word.word_id.description
        let obj = realm.objects(Myword.self).filter(filterCon)

        if obj.count == 0 {
            
            /* 書き込み */
            let myword = Myword()
            myword.id  = word.word_id
            myword.comment1  = word.comment1
            myword.comment2  = word.comment2
            myword.user_id = common.getUserId()
            myword.auther_user_id = common.getUserId()
            myword.is_owner = 1
            myword.is_remembered = 0
            myword.is_bookmarked = 0
            if (word.image_path != "") {
                myword.image_path = Constants.URL_API + "image/" + common.getUserId().description + "/" + word.image_path
            }
            
            try! realm.write() {
                realm.add(myword)
            }

            
        }
        
    }
    
}

