//
//  CameraViewController.swift
//  FlipUr
//
//  Created by 竹野学 on 2017/06/27.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import UIKit
import AVFoundation

class CameraViewController: UIViewController ,AVCapturePhotoCaptureDelegate{

    var captureSesssion: AVCaptureSession!
    var stillImageOutput: AVCapturePhotoOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    

    @IBOutlet weak var cameraView: UIView!
    
    @IBAction func takeIt(_ sender: Any) {
        
        // フラッシュとかカメラの細かな設定
        let settingsForMonitoring = AVCapturePhotoSettings()
        settingsForMonitoring.flashMode = .auto
        settingsForMonitoring.isAutoStillImageStabilizationEnabled = true
        settingsForMonitoring.isHighResolutionPhotoEnabled = false
        // シャッターを切る
        stillImageOutput?.capturePhoto(with: settingsForMonitoring, delegate: self)

        
    }
    
        override func viewDidLoad() {
            super.viewDidLoad()

//            print("viewWillAppearrrr")
            
            //トップに戻るボタンを作成
            // 指定した画像とスタイルを指定してボタンを作成
            let leftButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "cancel"), style: UIBarButtonItemStyle.plain, target: self, action: "back")
            
            //        let leftButton = UIBarButtonItem(title: "戻る", style: UIBarButtonItemStyle.plain, target: self, action: "back")
            self.navigationItem.leftBarButtonItem = leftButton

            
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // キューを作成し、非同期でカメラ起動を行う
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            self.displayCamera()
        }
    
//        // 5秒後に実行
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(5)) {
//            //            self.doSomething()
//            self.displayCamera()
//        }
    }

    
    func back () {
        //前画面に戻る。
        self.navigationController?.popViewController(animated: true)
    }

//    //トップに戻るボタン押下時の呼び出しメソッド
//    func goTop() {
//        
//        //トップ画面に戻る。
//        self.navigationController?.popToRootViewController(animated: true)
//     
//    }
//    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

/**
 *デリゲート。カメラで撮影が完了した後呼ばれる。
 *JPEG形式でフォトライブラリに保存。
 */
extension CameraViewController {
    
    func capture(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?, previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        if let photoSampleBuffer = photoSampleBuffer {
            // JPEG形式で画像データを取得
            let photoData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer, previewPhotoSampleBuffer: previewPhotoSampleBuffer)
            let image = UIImage(data: photoData!)
            
            let squareImage = image?.croppingToCenterSquare()
            // フォトライブラリに保存
            UIImageWriteToSavedPhotosAlbum(squareImage!, nil, nil, nil)
            
            
            //Navigation Controllerを取得
            let nav = self.navigationController!
            
            //呼び出し元のView Controllerを遷移履歴から取得しパラメータを渡す
            let viewVc = nav.viewControllers[nav.viewControllers.count-2] as! RegisterViewController
            viewVc.is_image_selected = true
            viewVc.imageView.image = squareImage
            
            //閉じる
            self.navigationController?.popViewController(animated: true)
        }
    }
    

}


extension CameraViewController {
    
    func displayCamera() {
        
        // ナビゲーションを透明にする処理
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        
        
        captureSesssion = AVCaptureSession()
        stillImageOutput = AVCapturePhotoOutput()
        
        captureSesssion.sessionPreset = AVCaptureSessionPreset1920x1080 // 解像度の設定
        
        //        let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        // 背面・前面カメラの選択
        let device = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera,
                                                   mediaType: AVMediaTypeVideo,
                                                   position: .back) // position: .front
        
        
        do {
            let input = try AVCaptureDeviceInput(device: device)
            
            // 入力
            if (captureSesssion.canAddInput(input)) {
                captureSesssion.addInput(input)
                
                // 出力
                if (captureSesssion.canAddOutput(stillImageOutput)) {
                    captureSesssion.addOutput(stillImageOutput)
                    captureSesssion.startRunning() // カメラ起動
                    
                    previewLayer = AVCaptureVideoPreviewLayer(session: captureSesssion)
                    
                    //                    previewLayer?.videoGravity = AVLayerVideoGravityResizeAspect // アスペクトフィット
                    previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill // アスペクトフィット
                    
                    previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.portrait // カメラの向き
                    
                    cameraView.layer.addSublayer(previewLayer!)
                    // ビューのサイズの調整
                    previewLayer?.frame = CGRect(x:0,y:0,width:UIScreen.main.bounds.width,height:UIScreen.main.bounds.width)
                    
                    
                    // ビューのサイズの調整
                    //                    previewLayer?.position = CGPoint(x: self.cameraView.frame.width / 2, y: self.cameraView.frame.height / 2)
                    //                    previewLayer?.position = CGPoint(x: self.cameraView.frame.width, y: self.cameraView.frame.height)
                    //                    previewLayer?.bounds = cameraView.frame
                    //                    self.previewLayer?.frame = cameraView.frame.bounds
                }
            }
        }
        catch {
            print(error)
        }

    }
    
}

extension UIImage {
    func croppingToCenterSquare() -> UIImage {
        let cgImage = self.cgImage!
        var newWidth = CGFloat(cgImage.width)
        var newHeight = CGFloat(cgImage.height)
        if newWidth > newHeight {
            newWidth = newHeight
        } else {
            newHeight = newWidth
        }
        let x = (CGFloat(cgImage.width) - newWidth)/2
        let y = (CGFloat(cgImage.height) - newHeight)/2
        let rect = CGRect(x: x, y: y, width: newWidth, height: newHeight)
        let croppedCGImage = cgImage.cropping(to: rect)!
        return UIImage(cgImage: croppedCGImage, scale: self.scale, orientation: self.imageOrientation)
    }
}
