//
//  DataStore.swift
//  MyUiScrollView
//
//  Created by 竹野学 on 2017/04/18.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift

class DeleteDataRepository {
    
    var word = Word()
    var usecase: WordUsecaseDelegate
    var common:Common
    
    init(usecase:WordUsecaseDelegate) {
        self.usecase = usecase
        self.common = Common()
        self.common.setUserInfo()
    }
    
    /**
     *
     *サーバーがデータをRequestする
     */
    func deleteWord(word:Word){
        
        self.word = word
        
        //オンラインの場合
        if (Common.isInternetAvailable()) {
            registerDataFromOnLine()
        } else {
            print("offline")
        }
        
    }
    
    
    private func registerDataFromOnLine () {
        
        let params: [String: Any] = ["word_id":word.word_id,"user_id":common.getUserId()]
        
        Alamofire.request(Constants.URL_DELETE_WORDS, method: .post, parameters: params, encoding: URLEncoding.httpBody).responseJSON
            {
                response in
                
                
                
                self.loadedData(response: response)
                self.usecase.deleteDone()
        }
        
        
    }
    
    private func getDataFromOffLine () {
        
    }
    
    private func loadedData(response: DataResponse<Any>?){
        
        if let data = response?.data {
            
            let json = JSON(data)
            
            let resultCode = json["ResultCode"].int
            
            if (resultCode == 0) {
                
//                var results = json["Results"]
                self.deleteInRealm()
            }
            
        }
        
    }
    
    
}


/**
 * ローカル側にもデータを登録する
 */
extension DeleteDataRepository {
    
    func deleteInRealm() {
        
        let realm = try! Realm()
        
        let filterCon = "id = " + word.word_id.description
        let obj = realm.objects(Myword.self).filter(filterCon)
        
        if obj.count > 0 {
            
            try! realm.write() {
                realm.delete(obj)
                print("削除:delete")
            }
            
            let objects = realm.objects(Myword.self)
            print(objects.count.description+"個になりました。")
        }
        
        
        
    }
}
