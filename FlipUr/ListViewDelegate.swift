//
//  ListViewDelegate.swift
//  MyLang
//
//  Created by 竹野学 on 2017/05/17.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation
protocol ListViewDelegate {
    func onEdited(pageIndex:Int,word:Word)
//    func onDeleted(pageIndex:Int,word:Word)
    
}
