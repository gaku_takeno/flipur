//
//  PhotoView.swift
//  MyUiScrollView
//
//  Created by 竹野学 on 2017/04/18.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation
protocol WordViewDelegate {
    func onLoaded(words: [Word],is_last:Int)
    func onReloaded(words: [Word])
    func onRegistered(word: Word)
    func onEdited(word: Word)
    func onDeleted(pageIndex:Int,word: Word)
    func onBookMarked(word: Word,message:String)
    
    func onRemembered(word: Word,message:String)
    func onRemovedFromMyList(word: Word,message:String)
    
    
}
