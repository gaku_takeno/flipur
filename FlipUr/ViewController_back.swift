//
//  ViewController.swift
//  MyLang
//
//  Created by 竹野学 on 2017/05/06.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class ViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    var common:Common!
    
    var w:CGFloat = 0
    var h:CGFloat = 0
    
    
    // 描画開始の x,y 位置
    var px:CGFloat = 0
    var py:CGFloat = 0
    let paddingVal:CGFloat = 60
    
    @IBInspectable var padding: UIEdgeInsets = UIEdgeInsets(top: 4, left: 8, bottom: 4, right: 8)
    

    var presenter: PhotoPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        common = Common()
        common.setScreenSize()
        
        w = common.getWidth()
        h = common.getHeight()
        
        py = h/2
        
        
        presenter = PhotoPresenter(view: self)
        presenter?.loadPhotos()
        
        
//        setScrollView()
        
        // Do any additional setup after loading the view, typically from a nib.
        
//        let imageURL = URL(string: "https://www.pakutaso.com/shared/img/thumb/dashPAKU0522_TP_V.jpg")
//        
//        imageView.sd_setImage(with: imageURL)
        
        
    }

    private func setScrollView() {
        
        // 表示窓のサイズと位置を設定
        scrollView.center = self.view.center

        // スクロールの跳ね返り
        scrollView.bounces = false
        
        // スクロールバーの見た目と余白
        scrollView.indicatorStyle = .white
        
        // ScrollViewの中身を作る
        for i in 1 ..< 9 {
            let label = UILabel()
            //UILabelで複数行のテキストを表示する場合
            label.numberOfLines = 0;
            label.text = "ラベル\(i)\nががががが\nrererer\nががががが\nrererer\nががががが\nrererer\nががががが\nrererer\nががががが\nrererer\n"
            label.sizeToFit()
//            label.backgroundColor = UIColor.lightGray
            
            label.backgroundColor = UIColor(patternImage: UIImage(named:"nophoto")!)
//            label.backgroundColor?.setFill()
            let xPosition = px
//            var yPosition = py - label.frame.size.height/2
//            var yPosition = 0
//            yPosition -= UIApplication.shared.statusBarFrame.height
            
            var rect:CGRect = label.frame
            //padding 30を両サイドに与える
//            rect.size.width = w - paddingVal
            
            rect.size.width = w
            rect.size.height = h
            rect.origin = CGPoint(x: xPosition, y: 0)
            label.frame = rect
            
            label.textAlignment = .center // 中央揃え
//            label.textAlignment = .right // 右揃え
            
            
//            var contentSize = label.intrinsicContentSize
//            contentSize.height += padding.top + padding.bottom
//            contentSize.width += padding.left + padding.right
//            
//            label.center = CGPoint(x: 100, y: 6)
            scrollView.addSubview(label)
            px += (common.getWidth())
        }
        
        self.view.addSubview(scrollView)
        
        // UIScrollViewのコンテンツサイズを画像のtotalサイズに合わせる
        let nWidth:CGFloat = w * CGFloat(8)
        print(nWidth)
        scrollView.contentSize = CGSize(width: nWidth, height: h)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    /* 以下は UITextFieldDelegate のメソッド */
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // スクロール中の処理
//        print("didScroll")
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        // ドラッグ開始時の処理
//        print("beginDragging")
    }
    
    func  scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print(scrollView.currentPage)
        if (scrollView.currentPage > 5) {
            addpage(num: scrollView.currentPage)
        }
    }

}


extension ViewController {
    func addpage ( num: Int) {
        
        var num = num
        let label = UILabel()
        //UILabelで複数行のテキストを表示する場合
        label.numberOfLines = 0;
        label.text = "ラベル\nががががが\nrererer\nががががが\nrererer\nががががが\nrererer\nががががが\nrererer\nががががが\nrererer\n"
        label.sizeToFit()
        //            label.backgroundColor = UIColor.lightGray
        
        label.backgroundColor = UIColor(patternImage: UIImage(named:"nophoto")!)
        //            label.backgroundColor?.setFill()
        let xPosition = px
        //            var yPosition = py - label.frame.size.height/2
        //            var yPosition = 0
        //            yPosition -= UIApplication.shared.statusBarFrame.height
        
        var rect:CGRect = label.frame
        //padding 30を両サイドに与える
        //            rect.size.width = w - paddingVal
        
        rect.size.width = w
        rect.size.height = h
        rect.origin = CGPoint(x: xPosition, y: 0)
        label.frame = rect
        
        label.textAlignment = .center // 中央揃え
        //            label.textAlignment = .right // 右揃え
        
        
        //            var contentSize = label.intrinsicContentSize
        //            contentSize.height += padding.top + padding.bottom
        //            contentSize.width += padding.left + padding.right
        //
        //            label.center = CGPoint(x: 100, y: 6)
        scrollView.addSubview(label)
        px += (common.getWidth())
        
        
        num = num+1
        let nWidth:CGFloat = w * CGFloat(num)
        print(nWidth)
        scrollView.contentSize = CGSize(width: nWidth, height: h)
        
    }
}
extension UIScrollView {
    var currentPage: Int {
        return Int((self.contentOffset.x + (0.5 * self.bounds.width)) / self.bounds.width) + 1
    }
}




extension ViewController: PhotoViewDelegate {
    
    func onLoaded(photos:[Photo]) {
        
        print(photos[0].comment1)
        print(photos.count)
        
//        displayImage(photos:photos)
    }
    
}



//class PaddingLabel: UILabel {
//    
//    @IBInspectable var padding: UIEdgeInsets = UIEdgeInsets(top: 4, left: 8, bottom: 4, right: 8)
//    
//    override func drawText(in rect: CGRect) {
//        let newRect = UIEdgeInsetsInsetRect(rect, padding)
//        super.drawText(in: newRect)
//    }
//    
//    override var intrinsicContentSize: CGSize {
//        var contentSize = super.intrinsicContentSize
//        contentSize.height += padding.top + padding.bottom
//        contentSize.width += padding.left + padding.right
//        return contentSize
//    }
//    
//}
