//
//  RegisterViewController.swift
//  MyLang
//
//  Created by 竹野学 on 2017/05/09.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import UIKit

class ListViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var tableview: UITableView!
    var presenter: WordPresenter?
    var words = [Word]()
    var word = Word()
    var loadingObj:ShowLoadingBar?
    
    override func viewDidLoad() {

        super.viewDidLoad()
        
        let common = Common()
        common.setScreenSize()
        
        self.title = "My List"
        presenter = WordPresenter(view: self)
        presenter?.loadMyList(type:Constants.MYLIST_TYPE_NORMAL)
        
        tableview.frame.size.height = common.getHeight()
        tableview.frame.size.width = common.getWidth()
        
        
//        self.setLoadingBar()
        // クルクルスタート
        loadingObj = ShowLoadingBar(view:self.view)
        loadingObj?.setLoading()
        if (Common.isInternetAvailable()) {
        loadingObj?.ActivityIndicator.startAnimating()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //各セルの要素を設定する
    func tableView(_ table: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ListTableViewCell = table.dequeueReusableCell(withIdentifier: "TbCell") as! ListTableViewCell
        cell.setTableView(tb: self)
        cell.setCell(words[indexPath.row],index: indexPath.row)
        return cell
        
    }
    
    
    /// セルの個数を指定するデリゲートメソッド（必須）
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return words.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    //unwind
    @IBAction func exitToList(segue: UIStoryboardSegue) {
    }
    
}



/**
 * ワードリストを取得した後に実行されるコールバックメソッドを実装する。
 */
extension ListViewController: WordViewDelegate {
    
    func onLoaded(words:[Word],is_last: Int) {
        
        // クルクルストップ
        loadingObj?.ActivityIndicator.stopAnimating()
        
        self.words += words
        setScrollView()
    }
    
    func onReloaded(words:[Word]) {}
    func onRegistered(word:Word) {}
    func onEdited(word:Word) {}
    
    /**
     * Listからの削除の完了後、listViewを更新する。
     */
    func onDeleted(pageIndex:Int,word:Word) {
    
        Common.showAlert(v: self,option:1,message:Lang.COMPLETED_DELETE_FROM_MYLIST_TRANSACTION);
        words.remove(at: pageIndex)
        
        // 1秒遅らせてリロード
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1), execute: {
            self.tableview.reloadData()
        })

    }
    func onBookMarked(word: Word,message:String) {}
    func onRemembered(word:Word,message:String) {}
    func onRemovedFromMyList(word: Word, message: String) {}
    
    
    
    
}


/**
 * scrollviewの描画を行う。
 */
extension ListViewController {
    
    func setScrollView() {
        
        self.tableview.rowHeight = UITableViewAutomaticDimension
        self.tableview.estimatedRowHeight = 40
        self.tableview.reloadData()
        
    }
    
}

/**
 * 編集画面に遷移後、編集を完了させ、その処理後のコールバック。tableviewを更新する。
 */
extension ListViewController:ListViewDelegate {
    
    func onEdited(pageIndex:Int,word:Word) {
    
            let cellData = self.words[pageIndex]
            words.remove(at: pageIndex)
            words.insert(cellData, at: pageIndex)
            // 1秒遅らせてリロード
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1), execute: {
                self.tableview.reloadData()
            })

    }
}

