//
//  ViewController.swift
//  MyLang
//
//  Created by 竹野学 on 2017/05/06.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class ViewController: UIViewController, UIScrollViewDelegate {

    
    @IBOutlet weak var scrollView: UIScrollView!
    var common:Common!
    
    var w:CGFloat = 0
    var h:CGFloat = 0
    
    var words = [Word]()
    
    // 描画開始の x,y 位置
    var px:CGFloat = 0
    var py:CGFloat = 0
    let marginVal:CGFloat = 60
    let paddingVal:CGFloat = 10;
    var count = 0
    
    @IBInspectable var padding: UIEdgeInsets = UIEdgeInsets(top: 4, left: 8, bottom: 4, right: 8)
    

    var presenter: WordPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        common = Common()
        
        initilSetting()
        
//        presenter = WordPresenter(view: self)
//        presenter?.loadPhotos()
        
    }

    
    /**
     * 初期設定をする。
     */
    func initilSetting() {
        
        common.setScreenSize()
        
        w = common.getWidth()
        h = common.getHeight()
        
        
        // 表示窓のサイズと位置を設定
        scrollView.center = self.view.center
        
        // スクロールの跳ね返り
        scrollView.bounces = false
        
        // スクロールバーの見た目と余白
        scrollView.indicatorStyle = .white
        
        py = h/2
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    /**
     * スクロール中の処理
     * UITextFieldDelegate のメソッド
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        print("didScroll")
    }
    
    /**
     * ドラッグ開始時の処理
     * UITextFieldDelegate のメソッド
     */
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        //
//        print("beginDragging")
    }
    
    /**
     * ドラッグが終わったら実行する
     * UITextFieldDelegate のメソッド
     */
    func  scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if (scrollView.currentPage > count - 4) {
            presenter?.loadPhotos()
        }
    }

}


/**
 * scrollviewに追加するページを作成する。
 */
extension ViewController {
    
    func addpage ( index: Int) {
        
        let xPosition = px
        let contentWidth = w - marginVal
        
        //コンテナのViewを作成する ---------------------------------
        let view = UIView()
        var rect:CGRect = view.frame
        
        //padding 30を両サイドに与える
        rect.size.width = contentWidth
        rect.size.height = h
        rect.origin = CGPoint(x: xPosition+marginVal/2, y: 0)
        view.frame = rect
        view.backgroundColor = UIColor(patternImage: UIImage(named:"nophoto")!)
//        view.alignmentRect(forFrame: )
        
        //テキストを作成する ---------------------------------
        let labelview = UIView()

        let label = UILabel()
        //UILabelで複数行のテキストを表示する場合
        label.text = words[index].comment1
        label.numberOfLines = 0;
        label.sizeToFit()
        var rectLlabel1:CGRect = label.frame
        rectLlabel1.size.width = contentWidth-paddingVal*2
        rectLlabel1.origin = CGPoint(x: paddingVal, y: paddingVal)
        
        rectLlabel1.size.height = h/2
        label.frame = rectLlabel1
        label.font = UIFont(name: "HiraKakuProN-W3", size: 24)
//        label.sizeThatFits(CGSize(width: contentWidth, height: CGFloat.greatestFiniteMagnitude))
        
        var rectlabelview1:CGRect = labelview.frame
        rectlabelview1.size.width = contentWidth
        rectlabelview1.size.height = h/2
        rectlabelview1.origin.y = 0
        labelview.frame = rectlabelview1
        labelview.backgroundColor = UIColor.red
        
        labelview.addSubview(label)
        //labelviewをコンテナに追加する
        view.addSubview(labelview)
        
        
        //テキストを作成する ---------------------------------
        let label2 = UILabel()
        let label2view = UIView()

        //UILabelで複数行のテキストを表示する場合
        label2.numberOfLines = 0;
        label2.text = words[index].comment2
        label2.sizeToFit()
        //label2.textAlignment = .left // 左揃え
        label2.font = UIFont(name: "HiraKakuProN-W3", size: 24)
        var rectLlabel2:CGRect = label2.frame
        rectLlabel2.size.width = contentWidth-paddingVal*2
        rectLlabel2.origin = CGPoint(x: paddingVal, y: paddingVal)
        
        
        rectLlabel2.size.height = h/2
        
        label2.frame = rectLlabel2
        
        label2view.backgroundColor = UIColor.yellow
        var rectlabelview2:CGRect = label2view.frame
        rectlabelview2.size.height = h/2
        rectlabelview2.size.width = contentWidth
        rectlabelview2.origin = CGPoint(x: 0, y: rectlabelview1.height+10)
        label2view.frame = rectlabelview2
        
        label2view.addSubview(label2)
        //labelviewをコンテナに追加する
        view.addSubview(label2view)
        
        //コンテナをscrollviewに追加する
        scrollView.addSubview(view)
        
        px += (common.getWidth())
        
        
        
    }
}

/**
 * scrollviewの現在のページ数/ページindexを取得して返す。
 */
extension UIScrollView {
    //scrollviewの現在のページindex
    var currentPageIndex: Int {
        return Int((self.contentOffset.x + (0.5 * self.bounds.width)) / self.bounds.width)
    }
    //scrollviewの現在のページ数
    var currentPage: Int {
        return Int((self.contentOffset.x + (0.5 * self.bounds.width)) / self.bounds.width) + 1
    }
}

//
//
///**
// * ワードリストを取得した後に実行されるコールバックメソッドを実装する。
// */
//extension ViewController: WordViewDelegate {
//    
//    func onLoaded(words:[Word]) {
//        
//        self.words += words
//        count = self.words.count
//        
//        //scrollviewの描画を行う
//        setScrollView()
//
//    }
//    
//}


/**
 * scrollviewの描画を行う。
 */
extension ViewController {
    
    func setScrollView() {
        
        // ScrollViewの中身を作る
        for i in 0 ..< words.count {
            addpage(index: i)
        }
    
        self.view.addSubview(scrollView)
        
        // UIScrollViewのコンテンツサイズを画像のtotalサイズに合わせる
        let nWidth:CGFloat = w * CGFloat(count)
        scrollView.contentSize = CGSize(width: nWidth, height: h)
    }
    
}
