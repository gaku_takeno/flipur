//
//  MainViewController.swift
//  MyLang
//
//  Created by 竹野学 on 2017/05/09.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import UIKit
import RealmSwift

class RememberViewController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    
    var delegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate

//    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var selectedImage: UIImage?
    var presenter: WordPresenter?
    var common:Common!
    var words = [Word]()
    var count = 0
    var is_last:Int = 0
    var loadingObj:ShowLoadingBar?
    
    override func viewDidAppear(_ animated: Bool) {
        
        if (Common.getRememberStatusChange() == 1) {
            print("viewDidAppear-RememberViewController")
            presenter?.loadMyList(type:Constants.MYLIST_TYPE_REMEMBER)
            Common.setRememberStatusChange(val:0)
        }
        
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let config = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
        Realm.Configuration.defaultConfiguration = config
        
        // DBファイルのfileURLを取得
        //        if let fileURL = Realm.Configuration.defaultConfiguration.fileURL {
        //            try! FileManager.default.removeItem(at: fileURL)
        //        }
        
        
        if (delegate.rememberViewController == nil) {
            delegate.rememberViewController = self
        }
        
        
        common = Common()
        common.setScreenSize()
        
        presenter = WordPresenter(view: self)
        presenter?.setMyListType(type:Constants.MYLIST_TYPE_REMEMBER)
        presenter?.loadMyList(type:Constants.MYLIST_TYPE_REMEMBER)
        
        collectionView.frame.size.height = common.getHeight() - CGFloat(Constants.NAVIGATION_HEIGHT) - CGFloat(Constants.MENU_BAR_HEIGHT)
        collectionView.frame.size.width = common.getWidth()
        
        loadingObj = ShowLoadingBar(view:self.view)
        loadingObj?.setLoadingBar()
        if (Common.isInternetAvailable()) {
        loadingObj?.ActivityIndicator.startAnimating()
        }
        
    }
    
    
    //unwind
    @IBAction func exitToMain(segue: UIStoryboardSegue) {
        //        if (segue.identifier == "back") {
//        print("back!")
        //        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        // Cell はストーリーボードで設定したセルのID
        let collectionCell:CollectionViewMyListCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionMyListCell", for: indexPath) as! CollectionViewMyListCell
        collectionCell.setMode(mode:0)
        let word = words[(indexPath as NSIndexPath).row]
        word.indexPath = indexPath
        collectionCell.setCell(word: word,presenter:presenter!)
        
        return collectionCell
        
    }
    
    
    // Screenサイズに応じたセルサイズを返す
    // UICollectionViewDelegateFlowLayoutの設定が必要
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellSizeW:CGFloat = common.getWidth()
        let cellSizeH:CGFloat = common.getHeight() - CGFloat(Constants.NAVIGATION_HEIGHT) - CGFloat(Constants.MENU_BAR_HEIGHT)
        
        // 正方形で返すためにwidth,heightを同じにする
        return CGSize(width: cellSizeW, height: cellSizeH)
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        //最後の要素で且つ、続きのデータがある場合
        if indexPath.row == words.count - 1, self.is_last == 0 {
            presenter?.loadMyList(type:Constants.MYLIST_TYPE_REMEMBER)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // 要素数を入れる、要素以上の数字を入れると表示でエラーとなる
        return words.count;
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


/**
 * ワードリストを取得した後に実行されるコールバックメソッドを実装する。
 */
extension RememberViewController: WordViewDelegate {
    
    func onReloaded(words:[Word]) {}
    func onLoaded(words:[Word],is_last:Int) {
        
        self.is_last = is_last
        self.words = words
        count = self.words.count
//        print(count.description+"個:初め")
        //scrollviewの描画を行う
        setScrollView()
        
        // クルクルストップ
        loadingObj?.ActivityIndicator.stopAnimating()
        
        
    }
    func onRegistered(word:Word) {}
    func onEdited(word:Word){}
    func onDeleted(pageIndex:Int,word: Word) {}
    func onBookMarked(word:Word,message:String){
        
    }
    func onRemembered(word:Word,message:String) {}
    
    func onRemovedFromMyList(word: Word, message: String) {
        
        let index = (word.indexPath as NSIndexPath).row
        self.words.remove(at: index)
        
        count = self.words.count
        
        //scrollviewの描画を行う
        setScrollView()
        
        common.showAlert(v: self, option: 0, message: message)
        
    }
    
}


/**
 * scrollviewの描画を行う。
 */
extension RememberViewController {
    
    func setScrollView() {
        
        collectionView.reloadData()
    }
    
}
