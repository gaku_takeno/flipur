//
//  PhotoRepositoryImpl.swift
//  MyUiScrollView
//
//  Created by 竹野学 on 2017/04/18.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class WordUsecase{

    var presenter:WordPresenter
    var datastore:GetDataRepository?
    var registerDatastore:RegisterDataRepository?
    var editDatastore:EditDataRepository?
    var deleteDatastore:DeleteDataRepository?
    var bookMarkDatastore:BookMarkDataRepository?
    var rememberDatastore:RememberDataRepository?
    var removeFromMyListDataRepository:RemoveFromMyListDataRepository?
    
    
    
    init(presenter:WordPresenter) {
        self.presenter = presenter
        self.datastore = GetDataRepository(usecase: self)
        self.registerDatastore = RegisterDataRepository(usecase: self)
        self.editDatastore = EditDataRepository(usecase: self)
        self.deleteDatastore = DeleteDataRepository(usecase: self)
        self.bookMarkDatastore = BookMarkDataRepository(usecase: self)
        self.rememberDatastore = RememberDataRepository(usecase: self)
        self.removeFromMyListDataRepository = RemoveFromMyListDataRepository(usecase: self)
        
    }

    
    /**
     * 
     *サーバーにALL LIST データをRequestする
    */
    func getPhotos(is_reload:Int){
        
        //オンラインの場合
        if (Common.isInternetAvailable()) {
            self.datastore?.getDataFromOnLine (is_reload:is_reload)
        } else {
            self.datastore?.getDataFromOffLine()
        }
        
    }
    
    
    /**
     *
     *サーバーにMY LIST データをRequestする
     */
    func getMyList(type:Int){
        
        if (Common.isInternetAvailable()) {
            print("1")
    
            //fbログアウト中
            if Common.isLoggedInWithFacebook() == false {
                self.datastore?.getDataFromOffLine ()
        print("2")
            } else {
            //オンライン/ログインの場合
                self.datastore?.getMyListDataFromOnLine (type:type)
                print("3")
            }
            
        } else {
            print("4")
            self.datastore?.getMyListDataFromOffLine(type:type)
        }
        
    }
    

    /**
     *
     *サーバーにデータを登録する
     */
    func registerWords(word:Word){
        
        self.registerDatastore?.registerWord(word: word)
        
    }
    
    
    /**
     *
     *サーバーにデータを編集する
     */
    func editWords(word:Word){
        
        self.editDatastore?.editWord(word: word)
        
    }
    
    /**
     *
     *　データを削除する
     */
    func deleteWords(word:Word){
        
        self.deleteDatastore?.deleteWord(word: word)
        
    }

    
    
    /**
     * bookmarkする
     */
    func bookMarkWords(word:Word){
        
        self.bookMarkDatastore?.bookMarkWord(word: word)
        
    }
    
    
    /**
     * rememberする
     */
    func rememberWords(word:Word){
        
        self.rememberDatastore?.rememberWord(word: word)
        
    }
    
    /**
     * remove from My List する
     */
    func removeFromMyListWords(word:Word,list_type:Int){
        
        self.removeFromMyListDataRepository?.removeFromMyListWord(word: word,list_type:list_type)
        
    }

}


extension WordUsecase :WordUsecaseDelegate {
    func done(is_last:Int,words:[Word]) {
        self.presenter.loaded(words: words,is_last:is_last)
    }
    
    func loadedMyList(is_last:Int, words:[Word]) {
        
        var words = words
        if (words.count < 1) {
            let word = Word()
            word.is_no_data = 1
            words.append(word)
        }
        
        self.presenter.loaded(words: words,is_last:is_last)
    }

    
    func reloadDone(is_last:Int,words:[Word]) {
        self.presenter.reloaded(words: words,is_last:is_last)
    }

    func registerDone() {
        let word = self.registerDatastore?.word
        self.presenter.registered(word: word!)
    }
    func editDone() {
        let word = self.editDatastore?.word
        self.presenter.onEdited(word: word!)
    }
    func deleteDone() {
        let word = self.deleteDatastore?.word
        self.presenter.onDeleted(word: word!)
    }


    func bookMarkDone(message:String) {
        let word = self.bookMarkDatastore?.word
        self.presenter.onBookMarked(word: word!,message:message)
    }
    
    func rememberDone(message:String) {
        let word = self.rememberDatastore?.word
        self.presenter.onRemembered(word: word!,message:message)
    }
    
    func removeFromMyListDone(message:String) {
//        let word = self.removeFromMyListDataRepository?.word
//        self.presenter.onRemovedFromMyList(word: word!,message:message)
    }
}
