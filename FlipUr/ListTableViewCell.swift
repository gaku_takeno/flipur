import UIKit

class ListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var comment1: UILabel!
    
    @IBOutlet weak var comment2: UILabel!
    
//    @IBOutlet weak var thumbnaiImageView: UIImageView!
    @IBOutlet weak var thumbnaiImageView: UIImageView!
    
    @IBOutlet weak var updated_at: UILabel!
    
    var word:Word?
    let common:Common = Common()
    var pageIndex:Int = 0
 
    @IBOutlet weak var editButton: UIButton!
    var listViewDelegate :ListViewDelegate?
    var listview:ListViewController?
    
    
    func setTableView(tb: ListViewDelegate) {
        self.listViewDelegate = tb
        self.listview = self.listViewDelegate as! ListViewController
        
        common.setScreenSize()
    }
    
    @IBAction func moveToEditPage(_ sender: Any) {
        
        showOtherActionSheet()

    }
    
    
    
    func setCell(_ info :Word,index:Int) {
        
        self.pageIndex = index
        self.word = info
        self.comment1.text = info.comment1
        self.comment2.text = info.comment2
        self.updated_at.text = info.updated_at
        
        if (self.word?.image_path != "") {
            
//            print(comment1.description+":"+(self.word?.image_path.description)!)
            let imageURL = URL(string: (self.word?.image_path)!)
            thumbnaiImageView.sd_setImage(with: imageURL)
            
//        } else {
//            
//            let cellImage = UIImage(named: "background2")
//            thumbnaiImageView.image = cellImage
//                
//
        }
        
        thumbnaiImageView.layer.cornerRadius = thumbnaiImageView.frame.size.width / 2
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    /// Show a dialog with two custom buttons.
    func showOtherActionSheet() {
        
        
        let title = "Setting"
        let message = ""
        let cancelButtonTitle = "キャンセル"
        let editButtonTitle = "編集"
        let deleteButtonTitle = "削除"
        
        let alertController = DOAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        
        let editAction = DOAlertAction(title: editButtonTitle, style: .default) { action in
            
            self.moveToEditView()
            
        }
        
        let deleteAction = DOAlertAction(title: deleteButtonTitle, style: .default) { action in
            
            //項目を削除する
            self.onDelete()
//            NSLog("deleteAction")
        }
        
        // Create the actions.
        let cancelAction = DOAlertAction(title: cancelButtonTitle, style: .destructive) { action in
//            NSLog("cancelAction")
        }
        
        // Add the actions.
        alertController.addAction(editAction)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
        
        self.listview?.present(alertController, animated: true, completion: nil)
        
    }
    
    /**
     * 項目を削除する
     */
    func onDelete () {
    
        self.listview?.presenter?.deleteWords(v: self.word!,pageIndex:pageIndex)
    }
    
    
    /**
     * 編集画面へ遷移する
     */
    func moveToEditView () {
        
        let view = self.listViewDelegate as! ListViewController
        view.dismiss(animated: true, completion: nil)
        
        // storyboardのインスタンス取得
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let editViewController = storyboard.instantiateViewController(withIdentifier :"EditViewController") as! EditViewController
        
        editViewController.word = self.word!
        editViewController.pageIndex = self.pageIndex
        
        editViewController.listViewDelegate = self.listViewDelegate
        view.present(editViewController, animated: true, completion: nil)
//        NSLog("editAction")

    }
    
}
