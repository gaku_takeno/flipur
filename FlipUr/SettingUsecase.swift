//
//  PhotoRepositoryImpl.swift
//  MyUiScrollView
//
//  Created by 竹野学 on 2017/04/18.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class SettingUsecase{
    
    var presenter:SettingPresenter
    var reportDatastore:SettingReportDataRepository?
    var blockDatastore:SettingBlockDataRepository?
    
    
    init(presenter:SettingPresenter) {
        self.presenter = presenter
        self.reportDatastore = SettingReportDataRepository(usecase: self)
        self.blockDatastore = SettingBlockDataRepository(usecase: self)
        
    }
    
    
    
    /**
     * 通報する
     */
    func reportWords(word:Word){
        
        //オンラインの場合
        if (Common.isInternetAvailable()) {
            self.reportDatastore?.reportDataFromOnLine(word:word)
        }
        
    }
    
    
    /**
     * ブロックする
     */
    func blockWords(word:Word){
        
        //オンラインの場合
        if (Common.isInternetAvailable()) {
            self.blockDatastore?.blockDataFromOnLine(word: word)
        } else {
            print("offline")
        }

        
    }
    
}


extension SettingUsecase :SettingUsecaseDelegate {
    
    func reportDone(message:String) {
        let word = self.reportDatastore?.word
        self.presenter.onReported(word: word!, message: message)
    }
    
    func blockDone(message:String) {
        let word = self.blockDatastore?.word
//        print("block!")
        self.presenter.onBlocked(word: word!, message: message)
    }
}
