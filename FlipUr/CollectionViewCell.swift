//
//  CollectionViewCell.swift
//  MyLang
//
//  Created by 竹野学 on 2017/05/09.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import Foundation
import SDWebImage
import PathMenu

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var comment1: UILabel!
    @IBOutlet weak var comment2: UILabel!

//    @IBOutlet weak var comment1Frame: UIView!
//    @IBOutlet weak var comment2Frame: UIView!
    @IBOutlet weak var rememberButton: UIButton!
    
    @IBOutlet weak var bookmarkButton: UIButton!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    var word = Word()
    
    var presenter:WordPresenter?
    var settingPresenter:SettingPresenter?
    
    var page:MainViewController?

    var messageButton:UIButton!
    var tutorialButton:UIButton?
    
    var is_already_displayed:Int = 0
    var is_already_tutorial_displayed:Int = 0
    
    var settingMenu:PathMenu?
    
//    func setUiView (v:UIViewController) {
//        self.page = v as! MainViewController
//    }
    
    // ボタンが押された時に呼ばれるメソッド
    func buttonEvent(sender: UIButton) {
        
        if (Common.isInternetAvailable()) {
            self.presenter?.reloadAllList()
            self.messageButton.isHidden = true
            
        } else {
            Common.showAlert(v: self.presenter?.view as! UIViewController, option: 0, message: Lang.ERROR_OFFLINE)
        }
        
    }
    
    
    
    func setCell(word: Word,presenter:WordPresenter,settingPresenter:SettingPresenter) {
        
        let common = Common()
        common.setScreenSize()

        self.word = word
        self.presenter = presenter
        self.settingPresenter = settingPresenter
        
        if (messageButton == nil) {
            self.displayOffLineView(message:Lang.ERROR_OFFLINE)
        }
        
        
        if (!Common.isInternetAvailable()) {
            
            messageButton.isHidden = false
            comment1.isHidden = true
            comment2.isHidden = true
            backgroundImage.isHidden = true
            rememberButton.isHidden = true
            bookmarkButton.isHidden = true
            settingMenu?.isHidden = true
            
//            menu?.isHidden = true
            
        
        } else {

//            messageButton.isHidden = true
            comment1.isHidden = false
            comment2.isHidden = false
            backgroundImage.isHidden = false
            rememberButton.isHidden = false
            bookmarkButton.isHidden = false
            settingMenu?.isHidden = false
            
//            if let msgB = messageButton {
                messageButton.isHidden = true
//            }
//            menu?.isHidden = false
            
        }

        
        
        // 画像配列の番号で指定された要素の名前の画像をUIImageとする
        if (word.image_path != "") {
            
            let imageURL = URL(string: word.image_path)
            backgroundImage.sd_setImage(with: imageURL)
            
        } else {
            
            if (Common.isInternetAvailable()) {
                let imageURL = URL(string: Constants.URL_MAIN_DEFAULT_IMAGE_PATH)
                backgroundImage.sd_setImage(with: imageURL)
                
            } else {
                
                let cellImage = UIImage(named: "background2")
                backgroundImage.image = cellImage
                
            }
            
            
        }
        
        comment1.text = word.comment1
        comment2.text = word.comment2
        
        if (self.word.is_bookmarked == 1) {
            let image:UIImage!
            image = UIImage(named: "bookmarkActive")
            bookmarkButton.setImage(image, for: .normal)
            
        } else {
            let image:UIImage!
            image = UIImage(named: "bookmark")
            bookmarkButton.setImage(image, for: .normal)
            
        }
            
        if (self.word.is_remembered == 1) {
            let image:UIImage!
            image = UIImage(named: "remembered")
            rememberButton.setImage(image, for: .normal)
        
        } else {
            let image:UIImage!
            image = UIImage(named: "remember")
            rememberButton.setImage(image, for: .normal)
                
        }
        
        
        
        //設定ボタンを表示する
        if (Common.isLoggedInWithFacebook() && Common.isInternetAvailable() && self.is_already_displayed == 0) {
            self.setMyPathmenu()
            
        } else if (!Common.isLoggedInWithFacebook() && self.is_already_tutorial_displayed == 0) {
            self.setTutorialButton()
        
        }
        
        if (!Common.isLoggedInWithFacebook() && self.is_already_displayed == 1) {
            //ログアウトしてて、設定ボタンが表示されている場合、設定ボタンを削除する
            print("22222")
            self.settingMenu?.removeFromSuperview()
        
        }
        
        if (Common.isLoggedInWithFacebook() && self.is_already_tutorial_displayed == 1) {
            //ログインしてて、テュートリアルボタンが表示されている場合、テュートリアルボタンを削除する
            print("333333")
            tutorialButton?.removeFromSuperview()
        }
        
        
//        comment1.sizeToFit()
//        comment2.sizeToFit()
        
    }
    
    
    
    @IBAction func bookmark(_ sender: Any) {
        
        
        if (!Common.isInternetAvailable()) {
            Common.showAlert(v: self.presenter?.view as! UIViewController,option:0,message:Lang.ERROR_OFFLINE);
            
        //fbログイン中
        } else if Common.isLoggedInWithFacebook() == true {
            self.presenter?.bookMarkWords(v: word)
            let image:UIImage!
            image = UIImage(named: "bookmarkActive")
            bookmarkButton.setImage(image, for: .normal)
            
        } else {
            Common.showAlert(v: self.presenter?.view as! UIViewController,option:0,message:Lang.ERROR_PLEASE_LOGIN);
        }
        
    }
    
    @IBAction func remember(_ sender: Any) {
        
        if (!Common.isInternetAvailable()) {
            Common.showAlert(v: self.presenter?.view as! UIViewController,option:0,message:Lang.ERROR_OFFLINE);
            
        //fbログイン中
        } else if Common.isLoggedInWithFacebook() == true {
            self.presenter?.rememberWords(v: word)
            let image:UIImage!
            image = UIImage(named: "remembered")
            rememberButton.setImage(image, for: .normal)
            
        } else {
            Common.showAlert(v: self.presenter?.view as! UIViewController,option:0,message:Lang.ERROR_PLEASE_LOGIN);
        }
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}

/**
 * off line 時の表示方法
 */
extension CollectionViewCell {
    func displayOffLineView(message:String) {
        
        // UIButtonのインスタンス
        messageButton = UIButton()
        
        let screenSize: CGRect = UIScreen.main.bounds
        
        // ステータスバーの高さを取得
        let statusBarHeight: Int = Int(UIApplication.shared.statusBarFrame.height)
        
        
        // ボタンのテキスト設定
        messageButton.setTitle(message, for: .normal)
        
        // ボタンのテキスト色を指定（これがないと白い文字になるため背景と同化して見えない）
        messageButton.setTitleColor(UIColor.white, for: .normal)
        
        // タップしたときに呼び出すメソッド指定
        messageButton.addTarget(self, action: #selector(buttonEvent(sender:)), for: .touchUpInside)
        
        // 大きさの自動調節
        messageButton.sizeToFit()
        
        let screenWidth = Int(screenSize.width)
        let screenHeight = Int(screenSize.height)
        
        let buttonHeight = (screenHeight/2)-(statusBarHeight + Constants.NAVIGATION_HEIGHT + Int(messageButton.frame.size.height/2))
        
        let butttonWidth = (screenWidth/2)-(Int(messageButton.frame.size.width/2))
        var viewFrame:CGRect = messageButton.frame
        viewFrame.origin = CGPoint(x: butttonWidth, y: buttonHeight)
        
        // 画像の中心を画面の中心に設定
        messageButton.frame = viewFrame
        self.addSubview(messageButton)
        
    }

}

/**
 * チュートリアルボタンを真ん中に表示する
 */
extension CollectionViewCell {
    func setTutorialButton () {
        
        let screenSize: CGRect = UIScreen.main.bounds
        
        // ステータスバーの高さを取得
        let statusBarHeight: Int = Int(UIApplication.shared.statusBarFrame.height)

        tutorialButton = UIButton()
        let screenWidth = Int(screenSize.width)
        let screenHeight = Int(screenSize.height)
        
        let buttonHeight = (screenHeight/2)-(statusBarHeight + Constants.NAVIGATION_HEIGHT + Int((tutorialButton?.frame.size.height)!/2))
        
        let butttonWidth = (screenWidth/2)-(Int((tutorialButton?.frame.size.width)!/2))
//        button.frame.origin = CGPoint(x: butttonWidth, y: buttonHeight)

//        button.setTitle("ボタン", for: .normal)
        
        let image = UIImage(named: "tutorial")
        tutorialButton?.setImage(image, for: .normal)
        tutorialButton?.frame = CGRect(x:0, y:0, width:60, height:60)
        // ボタンの位置を指定する.
        tutorialButton?.layer.position = CGPoint(x: butttonWidth, y: buttonHeight)
        tutorialButton?.tag = 1
        self.addSubview(tutorialButton!)

//        print("ボタン")
        is_already_tutorial_displayed = 1

    }
}

/**
 * pathmenuを追加する
 */
extension CollectionViewCell {
    
    func setMyPathmenu () {
        
        let menuItemImage = UIImage(named: "alerm")!
        let menuItemHighlitedImage = UIImage(named: "alerm")!
        let starImage = UIImage(named: "alerm")!
        
        let menuItemImage2 = UIImage(named: "block")!
        let menuItemHighlitedImage2 = UIImage(named: "block")!
        let starImage2 = UIImage(named: "block")!
        
        let menuItemImage3 = UIImage(named: "share")!
        let menuItemHighlitedImage3 = UIImage(named: "share")!
        let starImage3 = UIImage(named: "share")!
        
        let menuItemImage4 = UIImage(named: "tutorials")!
        let menuItemHighlitedImage4 = UIImage(named: "tutorials")!
        let starImage4 = UIImage(named: "tutorials")!
        
        let starMenuItem1 = PathMenuItem(image: menuItemImage, highlightedImage: menuItemHighlitedImage, contentImage: starImage)
        let starMenuItem2 = PathMenuItem(image: menuItemImage2, highlightedImage: menuItemHighlitedImage2, contentImage: starImage2)
        let starMenuItem3 = PathMenuItem(image: menuItemImage3, highlightedImage: menuItemHighlitedImage3, contentImage: starImage3)
        let starMenuItem4 = PathMenuItem(image: menuItemImage4, highlightedImage: menuItemHighlitedImage4, contentImage: starImage4)
        
        let items = [starMenuItem1, starMenuItem2, starMenuItem3, starMenuItem4]
        
        
        //        print("ggg")
        //        let logo = UIImage(named: "logo")
        
        let startItem = PathMenuItem(image: menuItemImage,
                                     highlightedImage: menuItemImage,
                                     contentImage: UIImage(named: "setting"),
                                     highlightedContentImage: UIImage(named: "setting"))
        
        let rect:CGRect = CGRect(x:0, y:0, width:60, height:60)
        settingMenu = PathMenu(frame: rect, startItem: startItem, items: items)
        
        settingMenu?.animationDuration = 0.3 // アニメーション速度を0.3秒に
        //        menu.menuWholeAngle = CGFloat(M_PI / 2) // メニューの角度を90度に
        
        let y = (Int(self.frame.size.height/2) - Int(Constants.STATUS_BAR_HEIGHT))
        
        settingMenu?.startPoint = CGPoint(x: UIScreen.main.bounds.width/2, y: CGFloat(y))
//        menu.farRadius = 30.0
        settingMenu?.nearRadius = 90.0
        settingMenu?.endRadius = 100.0
        addSubview(settingMenu!)
        
        
        is_already_displayed = 1
        
        settingMenu?.delegate = self as PathMenuDelegate
        
        // 表示フレームを作成。CGSizeMake(最大幅, 最大高さ)
        //        let frame = CGSizeMake(50, 50)
        

    }
}




extension CollectionViewCell: PathMenuDelegate {
    func didSelect(on menu: PathMenu, index: Int) {

        //通報
        if (index == 0) {
            
            if (!Common.isInternetAvailable()) {
                Common.showAlert(v: self.settingPresenter?.view as! UIViewController,option:0,message:Lang.ERROR_OFFLINE);
                
                //fbログイン中
            } else if Common.isLoggedInWithFacebook() == true {
                
                showReportConfirm()
                
                
            } else {
                Common.showAlert(v: self.settingPresenter?.view as! UIViewController,option:0,message:Lang.ERROR_PLEASE_LOGIN);
            }
            

            
        } else if (index == 1) {
        //ブロック
            
            if (!Common.isInternetAvailable()) {
                Common.showAlert(v: self.settingPresenter?.view as! UIViewController,option:0,message:Lang.ERROR_OFFLINE);
                
                //fbログイン中
            } else if Common.isLoggedInWithFacebook() == true {
                
                showBlockConfirm()
                
                
            } else {
                Common.showAlert(v: self.settingPresenter?.view as! UIViewController,option:0,message:Lang.ERROR_PLEASE_LOGIN);
            }
            
        } else if (index == 2) {
            //shareの場合
            
            if (!Common.isInternetAvailable()) {
                Common.showAlert(v: self.settingPresenter?.view as! UIViewController,option:0,message:Lang.ERROR_OFFLINE);
            } else {
                self.addShareFunction()
            }
        
        
        } else if (index == 3) {
            //tutorialの場合
            
        }
        
    }
    
    func willStartAnimationOpen(on menu: PathMenu) {
        print("Menu will open!")
    }
    
    func willStartAnimationClose(on menu: PathMenu) {
        print("Menu will close!")
    }
    
    func didFinishAnimationOpen(on menu: PathMenu) {
        print("Menu was open!")
    }
    
    func didFinishAnimationClose(on menu: PathMenu) {
        print("Menu was closed!")
    }
}


/**
 * share機能
 */
extension CollectionViewCell {
    
    func addShareFunction () {
        
        // 共有する項目
        let shareText = word.comment1
        let shareWebsite = NSURL(string: Constants.URL_APPLE_SHARE)!
//        let shareImage = UIImage(named: "share")!
        let shareImage:UIImage = backgroundImage.image!
        
        let activityItems = [shareText, shareWebsite, shareImage] as [Any]
        
        // 初期化処理
        let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        
        // 使用しないアクティビティタイプ
        let excludedActivityTypes = [
            UIActivityType.message,
            UIActivityType.saveToCameraRoll,
            UIActivityType.print
        ]
        
        activityVC.excludedActivityTypes = excludedActivityTypes
        
        let mainView = self.presenter?.view as! MainViewController
        // UIActivityViewControllerを表示
        mainView.present(activityVC, animated: true, completion: nil)
        
    }
    
}

extension CollectionViewCell {
    func showBlockConfirm() {
        
        // アラートを作成
        let alert = UIAlertController(
            title: Lang.CONFIRM_TITLE,
            message: Lang.CONFIRM_BLOCK,
            preferredStyle: .alert)
        
        // アラートにボタンをつける
        alert.addAction(UIAlertAction(title: Lang.OK, style: .default, handler: { action in
            self.settingPresenter?.blockWords(v: self.word)
            
        }))
        alert.addAction(UIAlertAction(title: Lang.CANCEL, style: .cancel))
        
        let mainView = self.presenter?.view as! MainViewController
        // アラート表示
        mainView.present(alert, animated: true, completion: nil)
    }
    
    func showReportConfirm() {
        
        // アラートを作成
        let alert = UIAlertController(
            title: Lang.CONFIRM_TITLE,
            message: Lang.CONFIRM_REPORT,
            preferredStyle: .alert)
        
        // アラートにボタンをつける
        alert.addAction(UIAlertAction(title: Lang.OK, style: .default, handler: { action in
            self.settingPresenter?.reportWords(v: self.word)
            
        }))
        alert.addAction(UIAlertAction(title: Lang.CANCEL, style: .cancel))
        
        let mainView = self.presenter?.view as! MainViewController
        // アラート表示
        mainView.present(alert, animated: true, completion: nil)
    }
}

