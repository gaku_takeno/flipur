//
//  EditViewController.swift
//  MyLang
//
//  Created by 竹野学 on 2017/05/14.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import UIKit

class EditViewController: UIViewController {

    var pageIndex:Int = 0
    var word = Word()
    let common = Common()
    
    @IBOutlet weak var comment1: UITextField!
    @IBOutlet weak var comment2: UITextField!
    var listViewDelegate:ListViewDelegate!
    
//    var callBackAction: ((Bool, Word, Int) -> Void)?
    
    var presenter: WordPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter = WordPresenter(view: self)
        comment1.text = word.comment1
        comment2.text = word.comment2
    }

    @IBAction func onSubmittedEdit(_ sender: Any) {
        
        comment1.endEditing(true)
        comment2.endEditing(true)
        
        if (comment1.text?.isEmpty == false) , (comment2.text?.isEmpty == false) {
            
            word.comment1 = comment1.text!
            word.comment2 = comment2.text!
        
            presenter?.editWords(v: word)
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



/**
 * ワードリストを取得した後に実行されるコールバックメソッドを実装する。
 */
extension EditViewController: WordViewDelegate {
    
    func onReloaded(words:[Word]) {}
    func onLoaded(words:[Word],is_last:Int) {}
    func onEdited(word:Word){
        common.showAlert(v: self,option:1,message:Lang.COMPLETED_REGISTRATION_TRANSACTION);
        comment1.text = ""
        comment2.text = ""
//        self.callBackAction!(true,word,self.pageIndex)
        
        //ListTableViewCellにて、listからedit画面へ遷移する際に、list画面で実装したListViewDelegateをedit画面に渡している。
        //その予め渡されていた実装されずみのListViewDelegateのonEditedメソッドを実行している。ロジックの実態はListViewDelegateが実装されているEditViewController
        self.listViewDelegate.onEdited(pageIndex: self.pageIndex,word:word)
    }
    func onDeleted(pageIndex:Int,word: Word) {}

    func onRegistered(word:Word) {}
    func onBookMarked(word:Word,message:String) {}
    func onRemembered(word:Word,message:String) {}
    func onRemovedFromMyList(word: Word, message: String) {}
    
}
