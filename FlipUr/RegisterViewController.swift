//
//  RegisterViewController.swift
//  MyLang
//
//  Created by 竹野学 on 2017/05/12.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import UIKit
import FacebookLogin
import Alamofire
import SwiftyJSON


class RegisterViewController: UIViewController,
                                UIImagePickerControllerDelegate,
                                UINavigationControllerDelegate,
                                UITextFieldDelegate{

//    @IBOutlet weak var debugText: UILabel!
    
    @IBOutlet weak var comment1: UITextField!
    @IBOutlet weak var comment2: UITextField!
    
    @IBOutlet weak var submit: UIButton!
    
    @IBOutlet weak var cameraRoll: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    var is_image_selected:Bool = false
    
    var screenWidth:CGFloat = 0
    var screenHeight:CGFloat = 0
    let word = Word()
    
    var common:Common!
    
    var presenter: WordPresenter?

    
//    @IBOutlet weak var cameraButton: Button_Custom!
    
    @IBAction func onDisplayCameraRoll(_ sender: Any) {
        self.choosePicture()
    }
    
    @IBAction func onResetImage(_ sender: Any) {
        self.resetPicture()
    }
    
    @IBAction func onDisplayCamera(_ sender: Any) {
        
//        let cameraView = CameraViewController()
//        cameraView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//        self.present(cameraView, animated: true, completion: nil)
//        

        let viewController = CameraViewController()
        
//        viewController.displayCamera()
        viewController.modalPresentationStyle = .overCurrentContext
        viewController.view.backgroundColor = UIColor.clear
        present(viewController, animated: true, completion: nil)
        

    }
    override func viewDidAppear(_ animated: Bool) {
        
        if (word.comment1 != "") {
            self.comment1.text = word.comment1
        }
        if (word.comment2 != "") {
            self.comment2.text = word.comment2
        }
        
//        cameraButton.addTarget(self, action: "showCrossDissolve:", for:.touchUpInside)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        super.viewWillAppear(animated)
        
        //コピーされた文言を入力フォームにコピペする
        onCopyAndPast ()
        
        self.configureObserver()
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        self.removeObserver() // Notificationを画面が消えるときに削除
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "登録"
     
        imageView.image = UIImage(named: "default")
        
        let screenSize: CGRect = UIScreen.main.bounds
        
        self.screenWidth = screenSize.width
        self.screenHeight = screenSize.height

        
        if Common.isLoggedInWithFacebook() == false {
            
            comment1.isHidden = true
            comment2.isHidden = true
            submit.isHidden = true
            
        } else {
        
            common = Common()
            presenter = WordPresenter(view: self)
        
            // 背景色
            comment1.backgroundColor = UIColor(white: 0.9, alpha: 1)
        
            // 背景色
            comment2.backgroundColor = UIColor(white: 0.9, alpha: 1)
        
            // テキストを全消去するボタンを表示
            comment1.clearButtonMode = .always
            comment2.clearButtonMode = .always
            
        
        }
//        self.view.frame.size.width = common.getWidth()
        
    }

    
    /**
    * copy & pastする
    */
    private func onCopyAndPast () {
        let pasteboard = UIPasteboard.general
        if let copiedString = pasteboard.string {
            
            //---------------------------------------------------
            let patternJP = "[\\p{Han}\\p{Hiragana}\\p{Katakana}ー。、○？]+"
            let matchesJP = Common.matchesForRegexInText(regex: patternJP, text: copiedString)
            
            var stringJP = ""
            for i in 0..<matchesJP.count {
                stringJP = stringJP + matchesJP[i]
            }
            
            
            //---------------------------------------------------
            let patternENG = "[’\'a-zA-Z0-9\\s@.%?]+"
            let matchesENG = Common.matchesForRegexInText(regex: patternENG, text: copiedString)
            
            let patternNum = "^[0-9]+$"
            var stringENG = ""
            for i in 0..<matchesENG.count {
                let matchesNum = Common.matchesForRegexInText(regex: patternNum, text: matchesENG[i])
                if matchesNum.count > 0 {
                    continue
                }
                    stringENG = stringENG + matchesENG[i]
            }
            
            self.comment2.text = stringENG
            self.comment1.text = stringJP
            
            
            //stringに内容が入ってくる
            pasteboard.string = ""
            
        }

    }
    
    @IBAction func onSubmitButton(_ sender: Any) {
        
        if Common.isLoggedInWithFacebook() == true {
            comment1.endEditing(true)
            comment2.endEditing(true)
        
            if (comment1.text?.isEmpty == false) , (comment2.text?.isEmpty == false) {
           
                word.category_id = 1
                word.comment1 = comment1.text!
                word.comment2 = comment2.text!
                
                //写真アップロードデータを取得
                if let base64String = self.uploadImageData() {
                    word.base64StringImageData = base64String
                }
                
                presenter?.registerWords(v: word)
            
                
                
            } else {
            
                common.showAlert(v: self,option:0,message:Lang.ERROR_REQUIRED);
                print("値がありません。")
            }
            
        } else {
            
            common.showAlert(v: self,option:0,message:Lang.ERROR_PLEASE_LOGIN);
            
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        
//       
//    }
    
    }



/**
 * ワードリストを取得した後に実行されるコールバックメソッドを実装する。
 */
extension RegisterViewController: WordViewDelegate {
    
    func onRemovedFromMyList(word: Word, message: String) {}
    func onBookMarked(word: Word,message:String) {}
    func onRemembered(word: Word,message:String) {}
    func onReloaded(words:[Word]) {}
    func onLoaded(words:[Word],is_last:Int) {}
    func onEdited(word:Word){}
    func onDeleted(pageIndex:Int,word: Word) {}

    func onRegistered(word:Word) {
        
        common.showAlert(v: self,option:1,message:Lang.COMPLETED_REGISTRATION_TRANSACTION);
        comment1.text = ""
        comment2.text = ""
        imageView.image = UIImage(named: "default")
        
//        debugText.text = word.comment2
        self.word.base64StringImageData = ""
        
        //写真の選択状態から外す
        self.is_image_selected = false
        
        
    }
}



extension RegisterViewController {

    /**
     * 写真アップロード
     */
    func uploadImageData () -> String? {
        
        var base64StringResponse:String? = nil
        
        
//        if ((self.imageView.image ) != nil){
        
        //写真が選択されている場合、画像をbase64にエンコードする
        if (self.is_image_selected == true ) {
    
            let chosenImage = self.imageView.image!
    
            let h = 1000 * (self.screenHeight/self.screenWidth)
    
            let uiimage = Common.cropThumbnailImage(image: chosenImage,w: 1010,h: Double(h))
    
            let jpegCompressionQuality: CGFloat = 0.3 // Set this to whatever suits your purpose
            if let base64String = UIImageJPEGRepresentation(uiimage, jpegCompressionQuality)?.base64EncodedString() {
                // Upload base64String to your database
    
                base64StringResponse = base64String
                
            }
        }
        
        return base64StringResponse
    }
    
    /**
     * カメラロールから写真を選択する処理
    */
    func choosePicture() {
        
        word.comment1 = comment1.text!
        word.comment2 = comment2.text!
        
//        print(word.comment1)
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            // 写真を選ぶビュー
            let pickerView = UIImagePickerController()
            // 写真の選択元をカメラロールにする
            // 「.camera」にすればカメラを起動できる
            pickerView.sourceType = .photoLibrary
            // デリゲート
            pickerView.delegate = self
            // ビューに表示
            self.present(pickerView, animated: true)
        }
    }
    
    /**
     * 写真を選んだ後に呼ばれる処理
     */
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        // 選択した写真を取得する
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        self.comment1.text = word.comment1
        self.comment2.text = word.comment2
        
        
        // ビューに表示する
        self.imageView.image = image
        self.is_image_selected = true
        // 写真を選ぶビューを引っ込める
        self.dismiss(animated: true)
        
        print(word.comment1)
        
    }
    
    /**
     * 写真をリセットする処理
     */
    func resetPicture() {
        
        //写真が選択されている場合、画像をbase64にエンコードする
        if (self.is_image_selected == true ) {
            
        // アラートで確認
        let alert = UIAlertController(title: "確認", message: "画像を初期化してもよいですか？", preferredStyle: .alert)
        let okButton = UIAlertAction(title: "OK", style: .default, handler:{(action: UIAlertAction) -> Void in
            // デフォルトの画像を表示する
            self.imageView.image = UIImage(named: "default")
            self.is_image_selected = false
            
        })
        let cancelButton = UIAlertAction(title: "キャンセル", style: .cancel, handler: nil)
        // アラートにボタン追加
        alert.addAction(okButton)
        alert.addAction(cancelButton)
        // アラート表示
        present(alert, animated: true, completion: nil)
            
        }
        
    }
    
}


/**
 * UITextField, UITextViewがキーボードで隠れるのを防ぐ
 */
extension RegisterViewController {
    // Notificationを設定
    func configureObserver() {
        
        let notification = NotificationCenter.default
        notification.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notification.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // Notificationを削除
    func removeObserver() {
        
        let notification = NotificationCenter.default
        notification.removeObserver(self)
    }
    
    // キーボードが現れた時に、画面全体をずらす。
    func keyboardWillShow(notification: Notification?) {
        
        let rect = (notification?.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        let duration: TimeInterval? = notification?.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? Double
        UIView.animate(withDuration: duration!, animations: { () in
            let transform = CGAffineTransform(translationX: 0, y: -(rect?.size.height)!)
            self.view.transform = transform
            
        })
    }
    
    // キーボードが消えたときに、画面を戻す
    func keyboardWillHide(notification: Notification?) {
        
        let duration: TimeInterval? = notification?.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? Double
        UIView.animate(withDuration: duration!, animations: { () in
            
            self.view.transform = CGAffineTransform.identity
        })
    }
    
    // 改行ボタンを押した時の処理
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        // キーボードを隠す
//        textField.resignFirstResponder()
        
        comment1.resignFirstResponder() // キーボードを下げる
        comment2.resignFirstResponder() // キーボードを下げる
        return true
    }
    
    // クリアボタンが押された時の処理
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    // テキストフィールドがフォーカスされた時の処理
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
//        comment2.resignFirstResponder() // キーボードを下げる
        return true
    }
    
    // テキストフィールドでの編集が終わろうとするときの処理
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    

}

