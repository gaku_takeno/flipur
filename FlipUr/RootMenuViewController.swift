//
//  RootMenuViewController.swift
//  MyLang
//
//  Created by 竹野学 on 2017/05/16.
//  Copyright © 2017年 gakutakeno. All rights reserved.
//

import UIKit

import PagingMenuController

class RootMenuViewController: UIViewController {
    

    @IBOutlet weak var buttonLogo: UIButton!
    
    override func viewDidAppear(_ animated: Bool) {
    
        if (Common.getIconStatusChange() == 1) {
            
            if Common.isLoggedInWithFacebook() == true {
            
                let commmon = Common()
                commmon.setUserInfo()
                let urlpath = "https://graph.facebook.com/"+commmon.getSnsId()+"/picture?type=small"
            
                //アイコン画像を非同期で読み込む
                self.loadImage(urlString: urlpath)
        
            } else {
                
                let image = UIImage(named: "logo")
                self.buttonLogo.setImage(image, for: .normal)
                
            }

            
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let options = PagingMenuOptions()
        let pagingMenuController = PagingMenuController(options: options)
        pagingMenuController.view.frame.origin.y += CGFloat(Constants.NAVIGATION_HEIGHT)
        
        addChildViewController(pagingMenuController)
        view.addSubview(pagingMenuController.view)
        pagingMenuController.didMove(toParentViewController: self)
        
        let commmon = Common()
        
        if Common.isLoggedInWithFacebook() == true {
            commmon.setUserInfo()
            let urlpath = "https://graph.facebook.com/"+commmon.getSnsId()+"/picture?type=small"
            
            //アイコン画像を非同期で読み込む
            self.loadImage(urlString: urlpath)
            
        }

        Common.setIconStatusChange(val:0)
//        Common.setLoginStatusChange(val: 0)
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
}



/**
 * 画像を非同期で読み込む
 */
extension RootMenuViewController {
 
    func loadImage(urlString: String){
        
        let req = URLRequest(url: NSURL(string:urlString)! as URL,
                             cachePolicy: .returnCacheDataElseLoad,
                             timeoutInterval: Constants.CACHE_SEC);
        let conf =  URLSessionConfiguration.default;
        let session = URLSession(configuration: conf, delegate: nil, delegateQueue: OperationQueue.main);
        
        session.dataTask(with: req, completionHandler:
            { (data, resp, err) in
                if((err) == nil){ //Success
                    
                    let image = UIImage(data:data!)
                    self.buttonLogo.setImage(image, for: .normal)
                }
        }).resume();
    }
    

}


private struct PagingMenuOptions: PagingMenuControllerCustomizable {
    
//    var vc1:MainViewController
    
    fileprivate var componentType: ComponentType {
        return .all(menuOptions: MenuOptions(), pagingControllers: pagingControllers)
    }
    
    fileprivate var pagingControllers: [UIViewController] {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc1 = storyboard.instantiateViewController(withIdentifier :"MainViewController") as! MainViewController
//        vc1.title = "ALL"
//        let vc2 = storyboard.instantiateViewController(withIdentifier :"ListViewController") as! ListViewController
        //        let photoListStoryboard = UIStoryboard(name: "PhotoList", bundle: nil)
        //        let vc2 = photoListStoryboard.instantiateInitialViewController()!
//        vc2.title = "Yours"

        let vc2 = storyboard.instantiateViewController(withIdentifier :"BookmarkViewController") as! BookmarkViewController
        let vc3 = storyboard.instantiateViewController(withIdentifier :"RememberViewController") as! RememberViewController
        
        return [vc1, vc2, vc3]
    }
    
    fileprivate struct MenuOptions: MenuViewCustomizable {
        var displayMode: MenuDisplayMode {
            return .segmentedControl
        }
        var itemsOptions: [MenuItemViewCustomizable] {
            return [MenuItem1(), MenuItem2(),MenuItem3()]
        }
        
        var focusMode: MenuFocusMode {
            return .underline(height: 4.0, color: MyColors.MAIN_COLOR, horizontalPadding: 0.0, verticalPadding: 0.0)
        }
    }
    
    fileprivate struct MenuItem1: MenuItemViewCustomizable {
        var displayMode: MenuItemDisplayMode {
            return .text(title: MenuItemText(text: "ALL"))
        }
    }
    fileprivate struct MenuItem2: MenuItemViewCustomizable {
        var displayMode: MenuItemDisplayMode {
            return .text(title: MenuItemText(text: "BookMark"))
        }
    }
    fileprivate struct MenuItem3: MenuItemViewCustomizable {
        var displayMode: MenuItemDisplayMode {
            return .text(title: MenuItemText(text: "Remember"))
        }
    }
    
    
    

}
